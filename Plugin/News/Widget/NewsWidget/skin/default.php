<?php

$current_language = ipContent()->getCurrentLanguage();
$current_ln_code = $current_language->getCode();

$where = array('Lang' => $current_ln_code);
$list = ipDb()->selectAll(
        'news', '*', $where, ' ORDER BY `newsOrder` '
);

echo '<div class="news_list">';
foreach ($list as &$item) {
    if(!empty($item['Content'])) {
        $link = '<a class="title" href="/'. $current_ln_code . '/news/' . $item['Url'] . '/' . $item['id']. '">' . $item['Title'] . '</a>';
    } else {
        $link = $item['Title'];
    }
    if ($item['Enabled']) {
        if ($item['photo']) {
            echo '<div class="row">';
            echo '<div class="col col_3 img">';
            echo '<img src="/file/repository/news/' . $item['photo'] . '" alt="" />'; 
            echo '</div>';
            $discount='';
            if ($item['Type'] == "2") {
                $discount = '<span class="discount"></span>';
            }            
            echo '<div class="col col_9 text">';
            echo '<h3>'.$discount.''.$link.'<br>'
                    . '<span class="date">' .$item['DateModified'] . ', ' .$item['City'].'</span></h3>';
            echo $item['Summary'];
            echo '</div>';
            echo '</div>';
            echo '<div class="clear"></div>';
        } else {
            echo '<div class="row no_foto">';
            echo '<h3>'.$discount.'<a class="title" href="/' . $current_ln_code . '/news/' . $item['Url'] . '/' . $item['id'] . '">' . $item['Title'] . '</a> '
                    . '<span class="date">' . $item['DateModified'] . '</span></h3>';
            echo $item['Summary'];
            echo '</div>';
        }
    }
}
echo ipRenderWidget('BookingForm','','button');
echo '</div>';

?>

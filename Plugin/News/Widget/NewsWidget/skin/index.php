<?php

$current_language = ipContent()->getCurrentLanguage();
$current_ln_code = $current_language->getCode();

$where = array('Lang' => $current_ln_code);
$list = ipDb()->selectAll(
        'news', '*', $where, ' ORDER BY `newsOrder` LIMIT 3'
);

echo '<div class="row news_list">';
$nr = 0;
foreach ($list as &$item) {
    if ($item['Enabled']) {
        if ($item['photo']) {
            $nr++;
            echo '<div class="col col-xs-12 col-sm-12 col-md-4 col-lg-4 li bg' . $nr . '">';
            echo '<div class="li_inner">';
            echo '<img class="img" src="/file/repository/news/' . $item['photo'] . '" alt="" />';  
            if ($item['Type'] == "2") {
                echo '<div class="discount">';

                echo '</div>';
            }
            echo '<div class="text">';
            echo '<div class="text-top">';
            echo '<p class="date">' . $item['DateModified'] . '</p>';
            echo '<p class="city">' . $item['City'] . '</p>';
            echo '<div class="my_clear"><!-- --></div>';
            echo '</div>';
            echo '<h2><a class="title" href="/' . $current_ln_code . '/news/' . $item['Url'] . '/' . $item['id'] . '">' . $item['Title'] . '</a></h2>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        } else {
            
        }
    }
}
echo '</div>';

?>

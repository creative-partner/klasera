<?php
namespace Plugin\News\Widget\NewsWidget;

class Controller extends \Ip\WidgetController
{
    public function getTitle()
    {
        return __('News', 'ipAdmin');
    }
}
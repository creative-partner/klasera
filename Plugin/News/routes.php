<?php

/**
 * Point your web-browser to URL http://www.example.com/hellorouter. As a result, web browser displays the text 'Hello World!'.
 * URL http://www.example.com/hellorouter/John/Smith displays the text 'Hello John Smith!'.
 */
$routes['news{/newsUrl,newsID}'] = function ($newsUrl = '', $newsID = '') {

    $output = '';
    $title = '';

    if ($newsUrl != '' && $newsID != '') {

        $current_language = ipContent()->getCurrentLanguage();
        $current_ln_code = $current_language->getCode();

        $where = array('Lang' => $current_ln_code, 'id' => $newsID);
        $list = ipDb()->selectRow(
                'news', '*', $where, ' ORDER BY `newsOrder` '
        );

        //setTitle($list['Title']);

        $output .='
				<ul class="breadcrumb">
					<li><a href="https://www.klasera.lt/'.$current_ln_code.'/">'.__('Titulinis', 'News').'</a></li>
					<li><a href="https://www.klasera.lt/lt/bosch/specialus-pasiulymai/">'.__('Naujienos', 'News').'</a></li>
					<li class="active">'.$list['Title'].'</li>
				</ul>		
		';

        $output .='<h1 class="news-title">' . $list['Title'] . '</h1>';
        $title .=$list['Title'];
        $output .='<p>' . $list['DateModified'] . '</p>';
        $output .='<div class="news-text">' . $list['Content'] . '</div>';
        $output .='<div class="back-link"><a href="https://www.klasera.lt/lt/bosch/specialus-pasiulymai/">&lt; ' . __('Grįžti į sąrašą', 'News') . '</a></div>';
        $output .= ipRenderWidget('BookingForm','','button');
    } else {
        $output .= "<h1>News</h1>";
    }

    $response = new \Ip\Response\Layout();
    $response->setLayout(ipFile('Theme/Air/bosh-full.php'));

    $response->setTitle($title);
    //$response->setBreadcrumb($title);
    $response->setContent($output);

    //var_dump(ipConfig());
    return $response;

    //return $output;
};

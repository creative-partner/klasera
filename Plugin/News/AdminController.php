<?php


namespace Plugin\News;

class AdminController extends \Ip\GridController {

    protected function config() {
        return array(
            'title' => 'News',
            'table' => 'news',
            'deleteWarning' => 'Are you sure?',
            'sortField' => 'newsOrder',
            'createPosition' => 'top',
            'pageSize' => 10,
            'fields' => array(
                array(
                    'type' => 'Select',
                    'label' => 'Language',
                    'field' => 'Lang',
                    'values' => array(
                        array('lt', 'LT'),
                        array('en', 'EN'),
                        array('ru', 'RU')
                    )
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Type',
                    'field' => 'Type',
                    'values' => array(
                        array('1', 'Naujienos'),
                        array('2', 'Akcijos')
                    )
                ),                
                array(
                    'type' => 'RepositoryFile',
                    'label' => 'Photo',
                    'field' => 'photo',
                    'preview' => __CLASS__ . '::show_thumb'
                ),
                array(
                    'label' => 'Title',
                    'field' => 'Title',
                    'validators' => array('Required')
                ),
                array(
                    'label' => 'City',
                    'field' => 'City',
                    'preview' => false  
                ),                
                array(
                    'type' => 'RichText',
                    'label' => 'Summary',
                    'field' => 'Summary',
                    'preview' => false                   
                ),
                array(
                    'type' => 'RichText',
                    'label' => 'Content',
                    'field' => 'Content',
                    'preview' => false
                ),
                array(
                    'label' => 'Date Created',
                    'field' => 'DateModified'
                ),
                array(
                    'type' => 'Checkbox',
                    'label' => 'Use date validation',
                    'field' => 'DateValidCheck'
                ),               
                array(
                    'label' => 'Date Valid',
                    'field' => 'DateValid'
                ),                
                array(
                    'type' => 'Checkbox',
                    'label' => 'Enabled',
                    'field' => 'Enabled'
                )
            ),
            'afterCreate' => array($this, 'afterCreate'),
            'afterUpdate' => array($this, 'afterUpdate')
        );
    }

    public static function show_thumb($value = null, $recordData) {
        if ($value) {
            $img = '<img style="width: 100px;" src="/file/repository/news/' . $value . '" alt="" />';
        } else {
            $img = '';
        }
        return $img;
    }

    public function afterCreate($id, $newData) {

        //date stuff
        $news_date = date("Y-m-d");
        ipDb()->update('news', array('DateModified' => $news_date), array('id' => $id));
        
        $news_date2 = date("Y-m-d");
        ipDb()->update('news', array('DateValid' => $news_date2), array('id' => $id));        

        // url stuff

        $cyr = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 
            'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 
            'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 
            'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 
            'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 
            'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 
            'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 
            'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 
            'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'а', 'б', 'в', 
            'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 
            'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 
            'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я','„','“','?','!','"',',','.','%','`' );

        $lat = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 
            'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 
            'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 
            'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 
            'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 
            'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 
            'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 
            'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 
            'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'a', 'b', 'v', 'g', 'd', 
            'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 
            'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 
            'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya','','','','','','','','','');

        $news_url = str_replace($cyr, $lat, $newData['Title']);

        $news_url = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $news_url);
        $news_url = strtolower(str_replace(array(' ','/'), '-', $news_url));

        $where = array();
        $url_list = ipDb()->selectAll(
                'news', '*', $where, '  '
        );

        $if_exists = '0';
        foreach ($url_list as $key => $url_list_item) {
            if ($url_list_item['id'] != $id) {
                if ($url_list_item['Url'] == $news_url) {
                    $if_exists = '1';
                }
            }
        }

        $next_alias = 1;
        $next_alias_nr = 2;
        if ($if_exists == '1') {

            while ($next_alias_nr <= 10) {
                foreach ($url_list as $key => $url_list_item) {
                    if ($url_list_item['id'] != $id) {
                        if ($url_list_item['Url'] == $news_url . '-' . $next_alias) {
                            $next_alias++;
                        }
                    }
                }
                $next_alias_nr++;
            }

            ipDb()->update('news', array('Url' => $news_url . '-' . $next_alias), array('id' => $id));
        } else {
            ipDb()->update('news', array('Url' => $news_url), array('id' => $id));
        }


        // image stuff
        $where = array('id' => $id);
        $list = ipDb()->selectValue(
                'news', 'photo', $where, '  '
        );

        if ($list) {

            $tmp_img = ipFile('file/repository/' . $list);
            $slider_img = ipFile('file/repository/news/' . $list);
            if (!file_exists($slider_img)) {
                copy($tmp_img, $slider_img);
            }

            define('DESIRED_IMAGE_WIDTH', 390);
            define('DESIRED_IMAGE_HEIGHT', 290);

            $source_path = $slider_img;

            list($source_width, $source_height, $source_type) = getimagesize($source_path);

            if (DESIRED_IMAGE_WIDTH == $source_width && DESIRED_IMAGE_HEIGHT == $source_height) {
                
            } else {
                switch ($source_type) {
                    case IMAGETYPE_GIF:
                        $source_gdim = imagecreatefromgif($source_path);
                        break;
                    case IMAGETYPE_JPEG:
                        $source_gdim = imagecreatefromjpeg($source_path);
                        break;
                    case IMAGETYPE_PNG:
                        $source_gdim = imagecreatefrompng($source_path);
                        break;
                }

                $source_aspect_ratio = $source_width / $source_height;
                $desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;

                if ($source_aspect_ratio > $desired_aspect_ratio) {
                    $temp_height = DESIRED_IMAGE_HEIGHT;
                    $temp_width = (int) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
                } else {
                    $temp_width = DESIRED_IMAGE_WIDTH;
                    $temp_height = (int) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
                }

                $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
                imagecopyresampled(
                        $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
                );

                $x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
                $y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
                $desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
                imagecopy(
                        $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
                );

                header('Content-type: image/jpeg');
                imagejpeg($desired_gdim, $source_path, 90);
            }
        };
    }

    public function afterUpdate($id, $newData) {

        // url stuff

        $cyr = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 
            'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 
            'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 
            'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 
            'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 
            'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 
            'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 
            'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 
            'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'а', 'б', 'в', 
            'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 
            'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 
            'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я','„','“','?','!','"',',','.','%','`' );

        $lat = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 
            'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 
            'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 
            'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 
            'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 
            'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 
            'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 
            'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 
            'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'a', 'b', 'v', 'g', 'd', 
            'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 
            'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 
            'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya','','','','','','','','','');
        
        $news_url = str_replace($cyr, $lat, $newData['Title']);

        $news_url = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $news_url);
        $news_url = strtolower(str_replace(array(' ','/'), '-', $news_url));

        $where = array();
        $url_list = ipDb()->selectAll(
                'news', '*', $where, '  '
        );

        $if_exists = '0';
        foreach ($url_list as $key => $url_list_item) {
            if ($url_list_item['id'] != $id) {
                if ($url_list_item['Url'] == $news_url) {
                    $if_exists = '1';
                }
            }
        }

        $next_alias = 1;
        $next_alias_nr = 2;
        if ($if_exists == '1') {

            while ($next_alias_nr <= 10) {
                foreach ($url_list as $key => $url_list_item) {
                    if ($url_list_item['id'] != $id) {
                        if ($url_list_item['Url'] == $news_url . '-' . $next_alias) {
                            $next_alias++;
                        }
                    }
                }
                $next_alias_nr++;
            }

            ipDb()->update('news', array('Url' => $news_url . '-' . $next_alias), array('id' => $id));
        } else {
            ipDb()->update('news', array('Url' => $news_url), array('id' => $id));
        }

        // image stuff
        $where = array('id' => $id);
        $list = ipDb()->selectValue(
                'news', 'photo', $where, '  '
        );

        if ($list) {

            $tmp_img = ipFile('file/repository/' . $list);
            $slider_img = ipFile('file/repository/news/' . $list);
            if (!file_exists($slider_img)) {
                copy($tmp_img, $slider_img);
            }

            define('DESIRED_IMAGE_WIDTH', 390);
            define('DESIRED_IMAGE_HEIGHT', 290);

            $source_path = $slider_img;

            list($source_width, $source_height, $source_type) = getimagesize($source_path);

            if (DESIRED_IMAGE_WIDTH == $source_width && DESIRED_IMAGE_HEIGHT == $source_height) {
                
            } else {
                switch ($source_type) {
                    case IMAGETYPE_GIF:
                        $source_gdim = imagecreatefromgif($source_path);
                        break;
                    case IMAGETYPE_JPEG:
                        $source_gdim = imagecreatefromjpeg($source_path);
                        break;
                    case IMAGETYPE_PNG:
                        $source_gdim = imagecreatefrompng($source_path);
                        break;
                }

                $source_aspect_ratio = $source_width / $source_height;
                $desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;

                if ($source_aspect_ratio > $desired_aspect_ratio) {
                    $temp_height = DESIRED_IMAGE_HEIGHT;
                    $temp_width = (int) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
                } else {
                    $temp_width = DESIRED_IMAGE_WIDTH;
                    $temp_height = (int) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
                }

                $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
                imagecopyresampled(
                        $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
                );

                $x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
                $y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
                $desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
                imagecopy(
                        $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
                );

                header('Content-type: image/jpeg');
                imagejpeg($desired_gdim, $source_path, 90);
            }
        };
    }

}

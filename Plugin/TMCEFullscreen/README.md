## DsTMCEFullscreen Plugin ##

Allows TinyMCE editor to go into full screen when editing in modal dialog.

### Usage ###

Click the `fullscreen` button on the first toolbar.
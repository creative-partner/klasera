var originalConfigFunction = ipTinyMceConfig;

var ipTinyMceConfig = function () {
    var newConfig = originalConfigFunction();
    var originalInit = newConfig.init_instance_callback;

    newConfig.plugins += ', fullscreen';
    newConfig.toolbar1 += ' fullscreen';

    newConfig.init_instance_callback = function(editor) {
        if (originalInit) originalInit(editor);

        var $fullscreen = $('.mce-i-fullscreen').parent('button');
        var fullscreen = false;

        var $modal = $('.ipsCreateModal, .ipsUpdateModal');
        var $dialog = $modal.find('.modal-dialog');
        $fullscreen.on('click', function(event) {
            if (fullscreen) {
                $modal.css('overflow', 'auto');
                $modal.css('height', 'auto');

                $dialog.css('margin', '30px auto');
                $dialog.css('width', '');
            } else {
                $modal.css('overflow', 'hidden');
                $modal.css('height', '100%');

                $dialog.css('margin', '0 auto');
                $dialog.css('width', '100%');
            }

            fullscreen = !fullscreen;
        });
    };

    return newConfig;
};

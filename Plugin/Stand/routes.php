<?php

$routes[__('info-stendas','Products').'/{url}'.'/{id}'] = array(
    'name' => 'Stand.singleCarDeal',
    'controller' => 'SiteController',
    'action' => 'singleCarDeals',
);

$routes[__('info-stendas','Products').'/{id}'] = array(
    'name' => 'Stand.categoryCarDeal',
    'controller' => 'SiteController',
    'action' => 'categoryCarDeals',
);

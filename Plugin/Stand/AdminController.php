<?php
namespace Plugin\Stand;
class AdminController{
    /**
     * @ipSubmenu Stendas
     */
    public  function index(){

        $langs = array();
        $langs_tmp = ipContent()->getLanguages();
        foreach ($langs_tmp as $key=>$lang_tmp) {
            $langs[]= array($lang_tmp->getCode(), $lang_tmp->getCode());
        }

        $config = array(
            'title' => 'Stendas',
            'table' => 'stand',
            'deleteWarning' => 'Ar esate tikri?',
            'sortField' => 'productsOrder',
            'createPosition' => 'bottom',
            'pageSize' => 30,
            'fields' => array(
                array(
                    'label' => 'Pavadinimas',
                    'field' => 'pavadinimas',
                    'validators' => array('Required'),
                ),
                array(
                    'label' => 'Kaina',
                    'field' => 'kaina'
                ),
                array(
                    'label' => 'Pagaminimo metai',
                    'field' => 'metai'
                ),
                array(
                    'type' => 'RichText',
                    'label' => 'Aprasymas',
                    'field' => 'aprasymas',
                    'preview' => false
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Kuro tipas',
                    'field' => 'kuro_tipas',
                    'values' => array(
                        array('Dyzelinas', 'Dyzelinas'),
                        array('Benzinas', 'Benzinas'),
                        array('Benzinas / dujos', 'Benzinas / dujos'),
                        array('Benzinas / elektra', 'Benzinas / elektra'),
                        array('Elektra', 'Elektra'),
                        array('Dyzelinas / elektra', 'Dyzelinas / elektra'),
                        array('Dyzelinas / dujos', 'Dyzelinas / dujos'),
                        array('Bioetanolis', 'Bioetanolis'),
                        array('kita', 'kita')
                    ),
                    'validators' => array('Required'),
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Pavaru dezes tipas',
                    'field' => 'pavaru_deze',
                    'values' => array(
                        array('Automatinė', 'Automatine'),
                        array('Mechaninė', 'Mechanine'),
                        array('Automatinė 7 pavarų', 'Automatine 7 pavaru')
                    ),
                    'validators' => array('Required'),
                ),
                array(
                    'label' => 'Galingumas (kW)',
                    'field' => 'galingumas'
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Duru skaicius',
                    'field' => 'duru_skaicius',
                    'values' => array(
                        array('2/3', '2/3'),
                        array('4/5', '4/5'),
                        array('Kita', 'Kita')
                    ),
                    'validators' => array('Required'),
                ),
                array(
                    'label' => 'Rida',
                    'field' => 'rida'
                ),
                array(
                    'label' => 'Spalva',
                    'field' => 'spalva'
                ),
                array(
                    'type' => 'RepositoryFile',
                    'label' => 'Nuotrauka',
                    'field' => 'nuotrauka',
                    'preview' => __CLASS__ . '::rodyti_nuotrauka'
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Stendo požymis',
                    'field' => 'pozymis',
                    'values' => array(
                        array(0, "----"),array(1, "TOP"),array(2, "KITI")
                    ),
                ),
                array(
                    'type' => 'Checkbox',
                    'label' => 'Aktyvus?',
                    'field' => 'active',
                    'preview' => true
                ),
                array (
                    'label' => 'Galerija',
                    'type' => 'Grid',
                    'config' => array(
                        'title' => 'Galerija',
                        'connectionField' => 'car_id',
                        'sortField' => 'gallery_order',
                        'createPosition' => 'bottom',
                        'table' => 'stand_gallery',
                        'fields' => array(
                            array(
                                'label' => 'Nuotrauka',
                                'field' => 'photo',
                                'type' => 'RepositoryFile',
                            )
                        )
                    )
                ),
                array(
                    'label' => 'Kontaktai',
                    'type' => 'Tab'
                ),
                array(
                    'type' => 'RepositoryFile',
                    'label' => 'Nuotrauka',
                    'field' => 'kontaktai_foto',
                    'preview' => false
                ),
                array(
                    'label' => 'Vardas, pavardė',
                    'field' => 'kontaktai_vardas',
                    'preview' => false
                ),
                array(
                    'label' => 'Pareigos',
                    'field' => 'kontaktai_pareigos',
                    'preview' => false
                ),
                array(
                    'label' => 'Telefonas',
                    'field' => 'kontaktai_tel',
                    'preview' => false
                ),
                array(
                    'label' => 'El. paštas',
                    'field' => 'kontaktai_email',
                    'preview' => false
                ),
            ),
            'afterCreate' => array($this, 'afterCreate'),
            'afterUpdate' => array($this, 'afterCreate')

        );
        return ipGridController($config);
    }

    public static function rodyti_aprasyma($value=null, $recordData){
        if($value){
            $desc = $value;

        }else{
            $desc = '';
        }
        return $desc;
    }

    public static function rodyti_nuotrauka($value=null, $recordData)
    {
        if($value){
            $img = '<img style="width: 100px;" src="/file/repository/'.$value.'" alt="" />';
        }else{
            $img = '';
        }
        return $img;
    }

    //generate product url after create or update

    public static function afterCreate($id,$newData) {
        $product_url = strtolower(\Ip\Internal\Text\Specialchars::url($newData['pavadinimas']));
        ipDb()->update('stand', array('url' => $product_url), array('id' => $id));
    }
}



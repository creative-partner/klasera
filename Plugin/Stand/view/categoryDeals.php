<?php ipSetLayout('info-stand-category.php'); ?>

<div class="col-xs-6" style="margin-bottom:20px;">
    <a href="/lt/info-stendas"><span class="arrow">←</span>   Atgal į pasiūlymus</a>
</div>

<div class="col-xs-12">
    <?php if($id == 1): ?>
        <div class="deal-title top">Top automobilis</div>
    <?php endif; ?>

    <?php if($id == 2): ?>
        <div class="deal-title">Kiti automobiliai</div>
    <?php endif; ?>
</div>

<?php foreach ($cars as $car): ?>
    <div class="col-md-6 item">
        <?php
        $options = array(
            'type' => 'center',
            'width' => 930,
            'height' => 470,
            'quality' => 95,
        );

        $thumbnail = ipReflection($car['nuotrauka'], $options);
        ?>
        <div class="photo">
            <a href="<?php echo ipRouteUrl('Stand.singleCarDeal', array('url' => $car['url'],'id' => $car['id'])); ?>">
                <img src="<?php echo ipFileUrl($thumbnail); ?>" alt="">
            </a>
        </div>
        <div class="desc">
            <div class="title">
                <h2><?php echo $car['pavadinimas'] ?></h2>
                <h3><?php echo $car['kaina'] . ' €' ?></h3>
            </div>
            <div class="info">
                <ul>
                    <?php if($car['metai']): ?><li><span>Pagaminimo metai:</span><?php echo $car['metai']; ?></li><?php endif; ?>
                    <?php if($car['kuro_tipas']): ?><li><span>Kuro tipas:</span><?php echo $car['kuro_tipas']; ?></li><?php endif; ?>
                    <?php if($car['pavaru_deze']): ?><li><span>Pavarų dėžės tipas:</span><?php echo $car['pavaru_deze']; ?></li><?php endif; ?>
                    <?php if($car['galingumas']): ?><li><span>Variklis:</span><?php echo $car['galingumas']; ?></li><?php endif; ?>
                    <?php if($car['rida']): ?><li><span>Rida</span><?php echo $car['rida']. ' km'; ?></li><?php endif; ?>
                    <?php if($car['spalva']): ?><li><span>Spalva:</span><?php echo $car['spalva']; ?></li><?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php ipSetLayout('info-stand-single.php'); ?>
<?php
    //print_r($car);
    //print_r($gallery);
    //print_r($othercars);
?>
<div class="col-md-12">
    <h1><?php echo $car['pavadinimas'] ?></h1>
</div>
<div class="col-md-6">
    <?php if($car['pozymis'] != 0): ?>
        <?php if($car['pozymis'] == 1): ?>
            <a href="/lt/info-stendas/1" class="deal-title top">Top automobilis</a>
        <?php endif; ?>

        <?php if($car['pozymis'] == 2): ?>
            <a href="/lt/info-stendas/2" class="deal-title">Kiti automobiliai</a>
        <?php endif; ?>
    <?php endif; ?>
    <?php
        $mainPhotos = '';
        $thumbPhotos = '';
        $bigPhotos = '';

        if($gallery){
            foreach ($gallery as $photo) {
                $imgFile = $photo["photo"];

                $options = array(
                    'type' => 'center',
                    'width' => 221,
                    'height' => 183,
                    'quality' => 80,
                    'forced' => false
                );
                $imgSmall = ipReflection($imgFile, $options);

                $options2 = array(
                    'type' => 'width',
                    'width' => 760,
                    'forced' => true
                );
                $imgBig = ipReflection($imgFile, $options2);

                $options3 = array(
                    'type' => 'fit',
                    'width' => 1920,
                    'height' => 1080,
                    'quality' => 80,
                    'forced' => false
                );
                $imgXBig = ipReflection($imgFile, $options3);


                $image = ipFileUrl($imgSmall);
                $imageBig = ipFileUrl($imgBig);
                $imageXBig = ipFileUrl($imgXBig);

                $mainPhotos .= '<a href="'.$imageXBig.'" target="_blank"><img alt="" src="'.$imageBig.'"> <img class="zoom" src="'.ipThemeUrl("assets/img/icon_zoom.png") .'" alt="Zoom"></a>';
                $thumbPhotos .= '<div><img alt=""src="'.$image.'"></div>';
            }
        }
    ?>
    <?php if($mainPhotos && $thumbPhotos): ?>
        <div class="slider_main"><?php echo $mainPhotos ?></div>
        <?php if(count($gallery) > 1): ?>
            <div class="slider_thumb"><?php echo $thumbPhotos ?></div>
        <?php endif; ?>
    <?php endif; ?>

    <a href="/lt/info-stendas/" class="close_button"><span class="arrow">←</span>   Atgal į pasiūlymus</a>
    <a href="#" class="close_button get_offer">Gauti individualų pasiūlymą</a>
</div>

<div class="col-md-6">
    <div class="price">Kaina: <?php echo $car['kaina']; ?> €</div>
    <table class="info">
        <tbody>
            <?php if($car['metai']): ?>
                <tr>
                    <td>Pagaminimo data:</td>
                    <td><?php echo $car['metai']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['rida']): ?>
                <tr>
                    <td>Rida:</td>
                    <td><?php echo $car['rida']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['kuro_tipas']): ?>
                <tr>
                    <td>Kuro tipas:</td>
                    <td><?php echo $car['kuro_tipas']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['pavaru_deze']): ?>
                <tr>
                    <td>Pavarų dėžės tipas:</td>
                    <td><?php echo $car['pavaru_deze']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['spalva']): ?>
                <tr>
                    <td>Spalva:</td>
                    <td><?php echo $car['spalva']; ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>

    <div class="description">
        <?php echo $car['aprasymas']; ?>
    </div>
</div>

<div class="col-md-12">
    <div class="cars">
        <?php if($othercars): ?>
            <?php foreach($othercars as $car) :?>
                <div class="col-md-6 item">
                    <?php
                    $options = array(
                        'type' => 'center',
                        'width' => 930,
                        'height' => 470,
                        'quality' => 95,
                    );

                    $thumbnail = ipReflection($car['nuotrauka'], $options);
                    ?>
                    <?php if($car['pozymis'] != 0): ?>
                        <?php if($car['pozymis'] == 1): ?>
                            <a href="/lt/info-stendas/1" class="deal-title top">Top automobilis</a>
                        <?php endif; ?>

                        <?php if($car['pozymis'] == 2): ?>
                            <a href="/lt/info-stendas/2" class="deal-title">Kiti automobiliai</a>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="photo">
                        <a href="<?php echo ipRouteUrl('Stand.singleCarDeal', array('url' => $car['url'],'id' => $car['id'])); ?>">
                            <img src="<?php echo ipFileUrl($thumbnail); ?>" alt="">
                        </a>
                    </div>
                    <div class="desc">
                        <div class="title">
                            <h2><?php echo $car['pavadinimas'] ?></h2>
                            <h3><?php echo $car['kaina'] . ' €' ?></h3>
                        </div>
                        <div class="info">
                            <ul>
                                <?php if($car['metai']): ?><li><span>Pagaminimo metai:</span><?php echo $car['metai']; ?></li><?php endif; ?>
                                <?php if($car['kuro_tipas']): ?><li><span>Kuro tipas:</span><?php echo $car['kuro_tipas']; ?></li><?php endif; ?>
                                <?php if($car['pavaru_deze']): ?><li><span>Pavarų dėžės tipas:</span><?php echo $car['pavaru_deze']; ?></li><?php endif; ?>
                                <?php if($car['galingumas']): ?><li><span>Variklis:</span><?php echo $car['galingumas']; ?></li><?php endif; ?>
                                <?php if($car['rida']): ?><li><span>Rida</span><?php echo $car['rida']. ' km'; ?></li><?php endif; ?>
                                <?php if($car['spalva']): ?><li><span>Spalva:</span><?php echo $car['spalva']; ?></li><?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
        <?php endforeach; ?>
     </div>
    <?php endif; ?>
</div>

<?php if($form): ?>
    <div class="contact-form">
        <div class="row">
            <div class="close-form"></div>
            <div class="col-xs-6 bg" style="background-image:url(<?php echo ipThemeUrl('assets/img/form_bg_1.jpg') ?>)"></div>
            <div class="col-xs-6 form">
                <h3>Susisiekite su mumis</h3>
                <p>Užpildykite formą ir mes Jums pateiksime geriausią pasirinkto modelio pasiūlymą!</p>
                <?php echo $form->render(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

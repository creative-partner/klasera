<?php
namespace Plugin\Stand;

class Model {

    public static function getProductGallery($id) {
        return ipDb()->selectAll('stand_gallery','*',array('car_id' => $id),'ORDER BY `gallery_order` ASC');
    }


    public static function getAllCarDeals(){
        $table = ipTable('stand');
        $sql = "SELECT * FROM $table WHERE active=1 ORDER BY `productsOrder`";
        $items = ipDb()->fetchAll($sql);
        return $items;
    }

    public static function getSingleCarDeals($id){
        $table = ipTable('stand');
        $sql = "SELECT * FROM $table WHERE active=1 AND `id`=$id";
        $item = ipDb()->fetchRow($sql);
        return $item;
    }

    public static function getOtherCarDeals($id){
        $table = ipTable('stand');
        $sql = "SELECT * FROM $table WHERE active=1 AND id != $id ORDER BY `productsOrder`";
        $items = ipDb()->fetchAll($sql);
        return $items;
    }

    public static function getCategoryCarDeals($id){
        $table = ipTable('stand');
        $sql = "SELECT * FROM $table WHERE active=1 AND pozymis = $id ORDER BY `productsOrder`";
        $items = ipDb()->fetchAll($sql);
        return $items;
    }
}

?>

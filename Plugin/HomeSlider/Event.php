<?php

namespace Plugin\HomeSlider;

class Event {

    public static function ipBeforeController() {
        ipAddCss('assets/jquery.bxslider.css');
        ipAddCss('assets/style.css');
        ipAddJs('assets/jquery.easing.1.3.js');
        ipAddJs('assets/jquery.bxslider.min.js');
        ipAddJs('assets/script.js');
    }

}

<?php
/**
 */

namespace Plugin\HomeSlider\Grid;

class RepositoryImage extends \Ip\Internal\Grid\Model\Field\RepositoryFile{
    public function preview($recordData)
    {
        if ($this->fileLimit == 1) {
            $image = $recordData[$this->field];
            $imageData = \Ip\Internal\Repository\BrowserModel::instance()->getFile($image);
            return '<img src="'.$imageData['previewUrl'].'">';
        } else {
            $data = json_decode($recordData[$this->field]);
            if (is_array($data)) {
                $data = implode(', ', $data);
            }
            return esc($data);
        }
    }

    public function createField()
    {
        $field = new \Ip\Form\Field\RepositoryFile(array(
            'label' => $this->label,
            'name' => $this->field,
            'preview' => 'thumbnails'
        ));
        if ($this->fileLimit !== null) {
            $field->setFileLimit($this->fileLimit);
        }
        $field->setValue(json_decode($this->defaultValue));
        return $field;
    }

    public function updateField($curData)
    {
        $field = new \Ip\Form\Field\RepositoryFile(array(
            'label' => $this->label,
            'name' => $this->field,
            'preview' => 'thumbnails'
        ));
        if ($this->fileLimit !== null) {
            $field->setFileLimit($this->fileLimit);
        }

        if ($this->fileLimit == 1) {
            $field->setValue(array($curData[$this->field]));
        } else {
            $field->setValue(json_decode($curData[$this->field]));
        }
        return $field;
    }

} 
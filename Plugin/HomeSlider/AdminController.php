<?php
/**
 * Created by PhpStorm.
 * User: Nerijus
 * Date: 2014-08-18
 * Time: 19:33
 */

namespace Plugin\HomeSlider;



class AdminController { 

    public function index(){
        
		$config = array(
			'title' => 'Slides',
			'table' => 'home_slider',
			'sortField' => 'gridOrder',
			'createPosition' => 'top',
			'pageSize' => 100,
			'fields' => array(
				array(
					'type' => 'Text', 'field' => 'title', 'preview' => true,
					'label' => 'Title'),
                array(
					'type' => 'Textarea', 'field' => 'text', 'preview' => true,
					'label' => 'Text'),
				array(
					'type' => 'Text', 'field' => 'link', 'preview' => true,
					'label' => 'Link'),
				array(
					'type' => '\Plugin\HomeSlider\Grid\RepositoryImage',
					'label' => 'Image',
					'preview' => true,
					'field' => 'image',
					'validators' => array('Required')
				)
			)
		);
        return ipGridController($config);
    }
}
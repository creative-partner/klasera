<?php
/**
 * Created by PhpStorm.
 * User: Nerijus
 * Date: 2014-08-18
 * Time: 20:18
 */

namespace Plugin\HomeSlider;


class Slot {
    public static function homeSlider(){
        $items = ipDb()->selectAll('home_slider', '*');


        $items = array_map(function($item){
            $item['image'] = ipReflection($item['image'], array('type' => 'center', 'width' => 730, 'height' => 320));
            if (!$item['image']){
                ipReflectionException();
            }
            $item['image'] = ipFileUrl($item['image']);

            return $item;
        }, $items);
        return ipView('view/slider.php', array('items' => $items));
    }
}
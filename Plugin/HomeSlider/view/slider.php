 <div class="asdSlider_container" id="asdSlider">
    <ul class="home-slider">
        <?php foreach($items as $key => $item): ?>
            <li class="slider-item"> 
                <div class="slider-back"></div>
                <!-- Main Image --> 
                
                
                        <img class="asdSlider_image ipsImage" src="<?php echo escAttr( $item['image'] ); ?>" alt="<?php echo escAttr($item['title']); ?>" />
                    <div class="slider-text-wrapp"> 
                    <h1>
                        <?php echo $item['title'] ?>
                    </h1>
                
                <!-- Layer 2 -->
                
                    <p><?php echo $item['text'] ?></p>
                
                <!-- Layer 3 -->
                <?php if(!empty($item['link'])) { ?> <a href="#" class="slider-button book-submit"><?php if(ipContent()->getCurrentLanguage()->getId() == 1) {echo 'Order service';} else {echo 'Užsakyti paslaugą';} ?></a><?php } ?>
                </div>
                
                <!-- Layers -->           
                <!-- Layer 1 -->
                
                
            </li>
        <?php endforeach; ?>
    </ul>
</div>
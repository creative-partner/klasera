<?php
/**
 * Created by PhpStorm.
 * User: Marijus
 * Date: 3/26/14
 * Time: 4:45 PM
 */

namespace Plugin\BookingForm;


class Helper
{

    public static function createForm()
    {

        $form = new \Ip\Form();

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'name', 'label' => __('*Vardas Pavardė', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'number', 'label' => __('*Tel. numeris', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Email(
            array(
                'name' => 'email', 'label' => __('*El. paštas', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'model', 'label' => __('*Automobilio markė ir modelis', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'plate_number', 'label' => __('*Automobilio valstybinis Nr.', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'made_year', 'label' => __('*Automobilio gamybos metai', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'vin', 'label' => __('Automobilio kėbulo numeris', 'BookingForm'),
            ));
        $form->addField($field);

        $values = array(
            array('Automobilio patikra prieš techninę abžiūrą', 'Automobilio patikra prieš techninę abžiūrą'),
            array('Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)', 'Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)'),
            array(' Automobilio elektros sistemų diagnostika ir remontas', ' Automobilio elektros sistemų diagnostika ir remontas'),array(' Akumuliatoriaus patikra ir keitimas', 'Akumuliatoriaus patikra ir keitimas'),array(' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas', ' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas'),array('Stabdžių sistemos patikra', 'Stabdžių sistemos patikra'),array('Stabdžių sistemos remontas', 'Stabdžių sistemos remontas'),array('Ratų montavimas ir balansavimas', 'Ratų montavimas ir balansavimas'),array('Ratų suvedimas/geometrija', 'Ratų suvedimas/geometrija'),array('Apšvietimo sistema patikra ir remontas', 'Apšvietimo sistema patikra ir remontas'),array('Variklio patikra ir remontas', 'Variklio patikra ir remontas'),array('Kiti automobilio mechaniniai remonto darbai', 'Kiti automobilio mechaniniai remonto darbai')
        );

        $field = new \Ip\Form\Field\Checkboxes(
            array(
                'name' => 'services', 'label' => __('*Bosch serviso paslauga', 'BookingForm'), 'values' => $values,
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'date', 'label' => __('Pageidautina data ir laikas', 'BookingForm'),
            ));
        $form->addField($field);

        $values = array(
            array('Automobilį iš serviso pasiimsiu pats', 'Automobilį iš serviso pasiimsiu pats'),
            array('Noriu, kad mašina būtų paimta ir pristatyta', 'Noriu, kad mašina būtų paimta ir pristatyta')
        );

        $field = new \Ip\Form\Field\Radio(
            array(
                'name' => 'delivery_pick', 'label' => __('Automobilio paėmimas ir pristatymas', 'BookingForm'), 'values' => $values,
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'pick_address', 'label' => __('Paėmimo adresas', 'BookingForm'), 'class' => 'hidden-address',
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'delivery_address', 'label' => __('Pristatymo adresas', 'BookingForm'), 'class' => 'hidden-address',
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Textarea(
            array(
                'name' => 'customer_text', 'label' => __('Papildoma informacija', 'BookingForm'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Checkbox(
            array(
                'name' => 'privacy',
                'label' => __('Papildoma informacija', 'BookingForm'),
                'text' => '<p>Su privatumo politika <a class="privacy" href="#">susipažinau ir sutinku</a></p>',
                'validators' => array('Required'),
            ));
        $form->addField($field);

        // 'sa' means Site controller action.
        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'sa',
                'value' => 'BookingForm.submit', // `FormExample` site controller's `showSummary` action.
            ));
        $form->addField($field);
        $form->addField(new \Ip\Form\Field\Submit(array('name' => 'Uzsiregistruok','value' => 'Užsiregistruok','class' => 'submit-button')));

        return $form;
    }

    public static function createForm2()
    {

        $form = new \Ip\Form();

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'name', 'label' => __('*Vardas Pavardė', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'number', 'label' => __('*Tel. numeris', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Email(
            array(
                'name' => 'email', 'label' => __('*El. paštas', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'model', 'label' => __('*Automobilio markė ir modelis', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'plate_number', 'label' => __('*Automobilio valstybinis Nr.', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'made_year', 'label' => __('*Automobilio gamybos metai', 'BookingForm'), 'validators' => array('Required'),
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'vin', 'label' => __('Automobilio kėbulo numeris', 'BookingForm'),
            ));
        $form->addField($field);

        $values = array(
            array('Automobilio patikra prieš techninę abžiūrą', 'Automobilio patikra prieš techninę abžiūrą'),
            array('Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)', 'Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)'),
            array(' Automobilio elektros sistemų diagnostika ir remontas', ' Automobilio elektros sistemų diagnostika ir remontas'),array(' Akumuliatoriaus patikra ir keitimas', 'Akumuliatoriaus patikra ir keitimas'),array(' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas', ' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas'),array('Stabdžių sistemos patikra', 'Stabdžių sistemos patikra'),array('Stabdžių sistemos remontas', 'Stabdžių sistemos remontas'),array('Ratų montavimas ir balansavimas', 'Ratų montavimas ir balansavimas'),array('Ratų suvedimas/geometrija', 'Ratų suvedimas/geometrija'),array('Apšvietimo sistema patikra ir remontas', 'Apšvietimo sistema patikra ir remontas'),array('Variklio patikra ir remontas', 'Variklio patikra ir remontas'),array('Kiti automobilio mechaniniai remonto darbai', 'Kiti automobilio mechaniniai remonto darbai')
        );

        $field = new \Ip\Form\Field\Checkboxes(
            array(
                'name' => 'services', 'label' => __('*Bosch serviso paslauga', 'BookingForm'), 'values' => $values,
            ));

        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'date', 'label' => __('Pageidautina data ir laikas', 'BookingForm'),
            ));
        $form->addField($field);

        $values = array(
            array('Automobilį iš serviso pasiimsiu pats', 'Automobilį iš serviso pasiimsiu pats'),
            array('Noriu, kad mašina būtų paimta ir pristatyta', 'Noriu, kad mašina būtų paimta ir pristatyta')
        );

        $field = new \Ip\Form\Field\Radio(
            array(
                'name' => 'delivery_pick', 'label' => __('Automobilio paėmimas ir pristatymas', 'BookingForm'), 'values' => $values,
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'pick_address', 'label' => __('Paėmimo adresas', 'BookingForm'), 'class' => 'hidden-address',
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'delivery_address', 'label' => __('Pristatymo adresas', 'BookingForm'), 'class' => 'hidden-address',
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Textarea(
            array(
                'name' => 'customer_text', 'label' => __('Papildoma informacija', 'BookingForm'),
            ));
        $form->addField($field);

        // 'sa' means Site controller action.
        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'sa',
                'value' => 'BookingForm.save', // `FormExample` site controller's `showSummary` action.
            ));
        $form->addField($field);

        // Submit button
        $form->addField(new \Ip\Form\Field\Submit(array('name' => 'Uzsiregistruok','value' => 'Užsiregistruok')));

        return $form;
    }

}

<?php
namespace Plugin\BookingForm;

class SiteController extends \Ip\Controller
{

    public function index()
    {

    }
    
    //Formos generavimas
    
    public function showForm()
    {

        $form = Helper::createForm();
        $data['form'] = $form;
        $renderedHtml = ipView('view/form.php', $data)->render();

        return $renderedHtml;
    }
    
    public function submit() {
        $text = 'Labas';
        return new \Ip\Response\Json( array('exampleMessage' => 'Hello world'));
    }
    
     
    
    //Email siuntimo funkcija 
    
    public function sendEmail($item) {
        $senderEmail = ipGetOptionLang('Config.websiteEmail');
        $services = json_decode($item['service']);
        $services = implode(",", $services);
        $mail = "Jūs gavote užsakymą. Kliento informacija:<br> 
        Vardas ir pavardė: ". $item['name'] ."<br>
        Telefono numeris: " . $item['number']. "<br>
        El. paštas: ". $item['email']. "<br>
        Automobilio markė ir modelis: ". $item['model']. "<br>
        Valstybinis numeris: ". $item['plate_number']. "<br>
        Automobilio gamybos metai: ". $item['made_year']. "<br>
        Automobilio kėbulo numeris: ". $item['vin']. "<br>
        Užsakytos paslaugos: ". $services. "<br>
        Pageidaujama data: ". $item['date']. "<br>
        Automobilio paėmimo ir pristaymo informacija: ".$item['delivery_pick']."<br>
        Paėmimo adresas: ".$item['pick_address']."<br>
        Pristatymo adresas: ".$item['delivery_address']."<br>
        Papildoma informacija: ".$item['customer_text']."<br>
        ";

        ipSendEmail($senderEmail, "Klasera", "bosch@klasera.lt", "Bosch", "Paslaugos užsakymas", $mail);
        ipSendEmail($senderEmail, "Klasera", "dalia.pleskoviene@baltic-auto.lt", "Bosch", "Paslaugos užsakymas", $mail);
        //ipSendEmail($senderEmail, "Klasera", "mindaugas@cpartner.lt", "Bosch", "Paslaugos užsakymas", $mail);
        //ipSendEmail($senderEmail, "Klasera", "irma@cpartner.lt", "Bosch", "Paslaugos užsakymas", $mail); 
    }
    
    //Išsaugomas užsakymas jei nėra klaidų
    
    public function save()
    {
        $form = Helper::createForm2();
        $postData = ipRequest()->getPost();
        $errors = $form->validate($postData);
        
        if ($errors) {
            // Validation error

            $status = array('status' => 'error', 'errors' => $errors);

            return new \Ip\Response\Json($status);
        } else {
            // Success
             $services = json_encode(ipRequest()->getPost('services'));
             $item = array(
                        'name' => ipRequest()->getPost('name'),
                        'number' => ipRequest()->getPost('number'),
                        'email' => ipRequest()->getPost('email'),
                        'model' => ipRequest()->getPost('model'),
                        'plate_number' => ipRequest()->getPost('plate_number'),
                        'made_year' => ipRequest()->getPost('made_year'),
                        'vin' => ipRequest()->getPost('vin'),
                        'service' => $services,
                        'date' => ipRequest()->getPost('date'),
                        'delivery_pick' => ipRequest()->getPost('delivery_pick'),
                        'pick_address' => ipRequest()->getPost('pick_address'),
                        'delivery_address' => ipRequest()->getPost('delivery_address'),
                        'customer_text' => ipRequest()->getPost('customer_text'),
                        'status' => 'new'
            );
            self::sendEmail($item);
            //$item['service'] = ipRequest()->getPost('services');
            Model::saveItemRecord($item);
            $actionUrl = ipActionUrl(array('sa' => 'BookingForm.showSuccessMessage'));
            $status = array('redirectUrl' => $actionUrl);
            return new \Ip\Response\Json($status);
    }
    }
    
    //Redirect'as jei užsakymas sėkmingas

    public function showSuccessMessage()
    {
        $renderedHtml = ipView('view/success.php')->render();

        return $renderedHtml;
    }


}

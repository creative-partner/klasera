<?php
namespace Plugin\BookingForm\Setup;

class Worker extends \Ip\SetupWorker
{

    public function activate()
    {
        $sql = '
        CREATE TABLE IF NOT EXISTS
           ' . ipTable('booking_form') . '
        (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255),
        `number` varchar(255),
        `email` varchar(255),
        `model` varchar(255),
        `plate_number` varchar(255),
        `made_year` varchar(255),
        `vin` varchar(255),
        `service` varchar(255),
        `date` varchar(255),
        `delivery_address` varchar(255),
        `pick_address` varchar(255),
        `delivery_pick` varchar(255),
        `customer_text` varchar(255),
        PRIMARY KEY (`id`)
        )';

        ipDb()->execute($sql);
    }

    public function deactivate()
    {
        $sql = 'DROP TABLE IF EXISTS ' . ipTable('booking_form');

        ipDb()->execute($sql);
    }

    public function remove()
    {

    }

}

<?php
namespace Plugin\BookingForm;

class AdminController extends \Ip\GridController
{

    protected function config()
    {
        return array(
            'title' => 'Booking list',
            'table' => 'booking_form',
            'deleteWarning' => __('Do you really want to delete this item?', 'BookingForm'),
            'createPosition' => 'top',
            'sortField' => 'id',
            'sortDirection' => 'desc',
            'allowSort' => false,
            'pageSize' => 25,
            'fields' => array(
                array(
                    'label' => __('Customer name', 'BookingForm'),
                    'field' => 'name'
                ),
                array(
                    'label' => __('Telephone number', 'BookingForm'),
                    'field' => 'number'
                ),
                array(
                    'label' => __('E-mail', 'BookingForm'),
                    'field' => 'email'
                ),
                array(
                    'label' => __('Car model', 'BookingForm'),
                    'field' => 'model'
                ),
                array(
                    'label' => __('Plate number', 'BookingForm'),
                    'field' => 'plate_number'
                ),
                array(
                    'label' => __('Year', 'BookingForm'),
                    'field' => 'made_year'
                ),
                array(
                    'label' => __('VIN number', 'BookingForm'),
                    'field' => 'vin'
                ),
                array(
                    'label' => __('Services', 'BookingForm'),
                    'field' => 'service', 'type' => 'Checkboxes', 'values' => array(array('Automobilio patikra prieš techninę abžiūrą', 'Automobilio patikra prieš techninę abžiūrą'),
            array('Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)', 'Sezoninė patikra (pavasario, vasaros, žiemos, atostogų)'),
            array(' Automobilio elektros sistemų diagnostika ir remontas', ' Automobilio elektros sistemų diagnostika ir remontas'),array(' Akumuliatoriaus patikra ir keitimas', 'Akumuliatoriaus patikra ir keitimas'),array(' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas', ' Kondicionavimo, vedinimo ir šildymo sistemos aptarnavimas ir remontas'),array('Stabdžių sistemos patikra', 'Stabdžių sistemos patikra'),array('Stabdžių sistemos remontas', 'Stabdžių sistemos remontas'),array('Ratų montavimas ir balansavimas', 'Ratų montavimas ir balansavimas'),array('Ratų suvedimas/geometrija', 'Ratų suvedimas/geometrija'),array('Apšvietimo sistema patikra ir remontas', 'Apšvietimo sistema patikra ir remontas'),array('Variklio patikra ir remontas', 'Variklio patikra ir remontas'),array('Kiti automobilio mechaniniai remonto darbai', 'Kiti automobilio mechaniniai remonto darbai'))
                ),
                array(
                    'label' => __('Time', 'BookingForm'),
                    'field' => 'date'
                ),
                array(
                    'label' => __('Car pick and delivery', 'BookingForm'),
                    'field' => 'delivery_pick', 'type' => 'Radio', 'values' => array( array('Automobilį iš serviso pasiimsiu pats', 'Automobilį iš serviso pasiimsiu pats'),
            array('Noriu, kad mašina būtų paimta ir pristatyta', 'Noriu, kad mašina būtų paimta ir pristatyta'))
                ),
                array(
                    'label' => __('Delivery address', 'BookingForm'),
                    'field' => 'delivery_address'
                ),
                array(
                    'label' => __('Pick address', 'BookingForm'),
                    'field' => 'pick_address'
                ),
                array(
                    'label' => __('Additional info', 'BookingForm'),
                    'field' => 'customer_text', 'type' =>'Textarea'
                ),
                array(
                    'type' => 'Select',
                    'label' => 'Status',
                    'field' => 'status',
                    'values' => array(
                        array('new', 'New'),
                        array('over', 'Over'),
                        array('cancelled', 'Cancelled')
                    )
                ),

            )
        );
    }

}
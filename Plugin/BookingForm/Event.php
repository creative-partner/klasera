<?php

namespace Plugin\BookingForm;
require('assets/mc_table.php');

class Event {

    public static function ipBeforeController() {
        ipAddJs('assets/moment.js');
        ipAddJs('assets/jquery.filthypillow.js');
        ipAddCss('assets/jquery.filthypillow.css');
        ipAddJs('assets/script.js');
        ipAddJs('assets/booking-form.js');
        ipAddCss('assets/style.css');
    }
    
    public static function ipCronExecute($info)
    {
        $info['lastTime']; //timestamp when cron had been executed for the last time
        //$info['test']; //flag if cron is running in a test mode
 
        if ($info['firstTimeThisDay'] || $info['test']){
            // Execute this code once a day
            
            //Get orders which has to be send
            $ordersToBeSend = Model::getOrdersToBeSend();
            
            if(!empty($ordersToBeSend)) {
                $reportPdf = self::generateReportPdf($ordersToBeSend);
                
                //if there is report which has to be sent, send it ant update orders sent field
                if(!empty($reportPdf)) {
                    self::sendReportEmail($reportPdf);
                    Model::updateSentOrders();
                }
            }
        }
    }
    
    //Generate new orders report
    
    public static function generateReportPdf($ordersToBeSend) {
        $pdf = new PDF_MC_Table();
        $pdf->AddPage();
        $pdf->AddFont('DejaVuSans','','DejaVuSansCondensed.php');
        $pdf->AddFont('DejaVuSansBold','','DejaVuSans-Bold.php');
        $pdf->SetWidths(array(50,100));
        $pdf->SetFont('DejaVuSansBold','', 15);
        $date = date('m/d/Y h:i:s a', time());
        $pdf->Cell(100,10,$date,0,1,'C');
        $pdf->Cell(100,10,'',0,1,'C');
        foreach($ordersToBeSend as $i => $order) {
            $services = array();
            $services = json_decode($order['service'],true);
            if(!empty($services)) {
                $services = implode(',',$services);
            }
            
            
            $pdf->SetTextColor(0); 
            $pdf->SetFillColor(36, 140, 129); 
            $pdf->SetFont('DejaVuSansBold','', 12);
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Užsakymo ID #').$order['id'],''));
            $pdf->SetFont('DejaVuSans','', 8);
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Vardas, Pavardė'),iconv('UTF-8', 'ISO-8859-4', $order['name'])));
            
            $pdf->Row(array('Tel. nr.',iconv('UTF-8', 'ISO-8859-4', $order['number'])));
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'El. paštas'),iconv('UTF-8', 'ISO-8859-4', $order['email'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Automobilio modelis'),iconv('UTF-8', 'ISO-8859-4', $order['model'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Valstybiniai numeriai'),iconv('UTF-8', 'ISO-8859-4', $order['plate_number'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Automobilio gamybos metai'),iconv('UTF-8', 'ISO-8859-4', $order['made_year'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Kėbulo nr.'),iconv('UTF-8', 'ISO-8859-4', $order['vin'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Užsakytos paslaugos'),iconv('UTF-8', 'ISO-8859-4', $services)));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Pageidaujama data'),iconv('UTF-8', 'ISO-8859-4', $order['date'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Pristatymo adresas'),iconv('UTF-8', 'ISO-8859-4', $order['delivery_address'])));
            
             $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Atsiėmimo adresas'),iconv('UTF-8', 'ISO-8859-4', $order['pick_address'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Kliento komentaras'),iconv('UTF-8', 'ISO-8859-4', $order['customer_text'])));
            
            $pdf->Row(array(iconv('UTF-8', 'ISO-8859-4', 'Užsakymo statusas'),iconv('UTF-8', 'ISO-8859-4', $order['status'])));
            
            $pdf->Row(array('',''));
            
            
        }
        $report = $pdf->Output('','S');
        
        return $attachment = chunk_split(base64_encode($report));
    }
    
    //Send generated report
    
    public static function sendReportEmail($reportPdf) {
        $eol = PHP_EOL;
        $separator = md5(time());
        $to = "klasera@klasera.lt"; 
        $to2 = "irma@cpartner.lt";
        $to3 = "kestutis@cpartner.lt";
        $to4 = "dalia.pleskoviene@baltic-auto.lt";
        $from = "klasera@klasera.lt"; 
        $subject = "Užsakymų ataskaita"; 
        $message = "<p>Užsakymų ataskaita</p>";

        $filename = "ataskaita.pdf";

        // main header (multipart mandatory)
        $headers  = "From: ".$from.$eol;
        $headers .= "MIME-Version: 1.0".$eol; 
        $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol; 
        $headers .= "Content-Transfer-Encoding: 7bit".$eol;
        $headers .= "This is a MIME encoded message.".$eol.$eol;
        // message
        $headers .= "--".$separator.$eol;
        $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
        $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
        $headers .= $message.$eol.$eol;
        
        // attachment
        $headers .= "--".$separator.$eol;
        $headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
        $headers .= "Content-Transfer-Encoding: base64".$eol;
        $headers .= "Content-Disposition: attachment".$eol.$eol;
        $headers .= $reportPdf.$eol.$eol;
        $headers .= "--".$separator."--";

        // send message
        mail($to, $subject, "", $headers);
        mail($to2, $subject, "", $headers);
        mail($to3, $subject, "", $headers);
        mail($to4, $subject, "", $headers);
    }

}

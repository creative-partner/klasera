<?php
namespace Plugin\BookingForm;

use Ip\Form\Exception;

class Model
{
    
    public static function saveItemRecord($item)
    {

        ipDb()->insert('booking_form', $item);

    }
    
    public static function getOrdersToBeSend() {
        $results = ipDb()->selectAll('booking_form', '*', array('sent' => 0),'ORDER BY id');
        return $results;
    }
    
    public static function updateSentOrders() {
        ipDb()->update('booking_form', array('sent' => 1), array('sent' => 0));
    }
}

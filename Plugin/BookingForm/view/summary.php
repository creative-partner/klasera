<?php $services = json_decode($item['service']); ?>
<?php $services = implode(",", $services); ?>

<h1>Užsakymo peržiūra</h1>
<p>Vardas ir pavardė: <?php echo $item['name']; ?></p>
<p>Telefono numeris: <?php echo $item['number']; ?></p>
<p>Elektroninis paštas: <?php echo $item['email']; ?></p>
<p>Automobilio modelis: <?php echo $item['model']; ?></p>
<p>Valstybinis numeris: <?php echo $item['plate_number']; ?></p>
<p>Automobilio gamybos metai: <?php echo $item['made_year']; ?></p>
<p>Kėbulo numeris: <?php echo $item['vin']; ?></p>
<p>Paslaugos: <?php echo $services; ?></p>
<p>Pageidaujama data: <?php echo $item['date']; ?></p>
<p>Automobilio paėmimo ir pristaymo informacija: <?php echo $item['delivery_pick']; ?></p>
<p>Paėmimo adresas: <?php echo $item['pick_address']; ?></p>
<p>Pristatymo adresas: <?php echo $item['delivery_address']; ?></p>
<p>Papildoma informacija: <?php echo $item['customer_text']; ?></p>
<a href="<?php echo ipRouteUrl('success',array('item'=>$item)); ?>">Užsiregistruoti</a>
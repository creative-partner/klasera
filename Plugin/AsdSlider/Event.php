<?php

namespace Plugin\AsdSlider;

class Event {

    public static function ipBeforeController() {
        ipAddCss('assets/jquery.bxslider.css');
        ipAddCss('assets/style.css');
        ipAddJs('assets/jquery.easing.1.3.js');
        ipAddJs('assets/jquery.bxslider.min.js');
        if( ipIsManagementState() ) {
            ipAddJs('assets/Slider.js');
        }
        if( !ipIsManagementState() ) {
            $script = "
                $(document).ready(function() {
                    if( typeof asdSliderList != 'undefined' && asdSliderList.length > 0 ) {
                        $.each( asdSliderList, function( key, value ) {
                            var options = {};
                            if( typeof value.options !== 'undefined' ) {
                                options.auto = true;
                                options.mode = value.options.mode;
                                options.captions = parseInt(value.options.captions) == 0 ? false : true;
                                options.pager = parseInt(value.options.page) == 0 ? false : true;
                            }
                            $( value.id ).bxSlider( options );
                        });
                    }
                });
            "; 
            ipAddJsContent('asdslider', $script);
        }
    }

}

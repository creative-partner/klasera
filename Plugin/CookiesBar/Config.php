<?php
/**
 * @package   ImpressPages
 */


namespace Plugin\CookiesBar;


class Config {
    public static function showCookies()
    {
        return ipGetOption('CookiesBar.showCookies');
    }
    public static function cookiesButton()
    {
        $current_language = ipContent()->getCurrentLanguage();
        $current_ln_code = $current_language->getCode();
        return ipGetOptionLang('CookiesBar.cookiesButton',$current_ln_code);
    }
    public static function cookiesMore()
    {
        $current_language = ipContent()->getCurrentLanguage();
        $current_ln_code = $current_language->getCode();
        return ipGetOptionLang('CookiesBar.cookiesMore',$current_ln_code);
    }
    public static function cookiesMessage()
    {
        $current_language = ipContent()->getCurrentLanguage();
        $current_ln_code = $current_language->getCode();
        return ipGetOptionLang('CookiesBar.cookiesMessage',$current_ln_code);
    }
}

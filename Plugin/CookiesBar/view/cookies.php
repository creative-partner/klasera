    window.addEventListener("load", function () {
        var containerToAppend = document.getElementById('cookiePolicyHolder');
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#133752",
                    "text": "#fff"
                },
                "button": {
                    "background": "#133752",
                    "text": "#fff",
                    "border": "#fff"
                }
            },
            "theme": "classic",
            "content": {
                "message": '<?php echo $cookiesMessage ?>',
                "dismiss": '<?php echo esc($cookiesButton) ?>',

            },
            elements: {
            	messagelink: '<span id="cookieconsent:desc" class="cc-message">{{message}}</span>'
        	},
            "container": containerToAppend
        })
    });






<?php
/**
 * @package   ImpressPages
 */


namespace Plugin\CookiesBar;


class Event
{
    public static function ipBeforeController()
    {
        // cookies message to show
        $showCookies = Config::showCookies();
        $cookiesButton = Config::cookiesButton();
        $cookiesMessage = Config::cookiesMessage();
        $cookiesMore = Config::cookiesMore();

        if ($showCookies == '0' || empty($cookiesButton) || empty($cookiesMessage) ){

        } else {

            ipAddJs('Plugin/CookiesBar/assets/cookies.js');

            //ipAddCss('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.6/cookieconsent.min.css');
            //ipAddCss('Plugin/CookiesBar/assets/override.css');
            //ipAddJs('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.6/cookieconsent.min.js');
            //$script = ipView('view/cookies.php', compact('cookiesButton', 'cookiesMessage', 'cookiesMore'))->render();
            //ipAddJsContent('CookiesBarScript', $script);
        }

    }

}

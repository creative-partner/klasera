<?php ipSetLayout('info-stand-single.php'); ?>
<?php
    //print_r($car);
    //print_r($gallery);
?>
<div class="col-md-12">
    <h1><?php echo $car['pavadinimas'] ?></h1>
</div>
<div class="col-md-6">
    <?php if($car['pozymis'] != 0): ?>
        <?php if($car['pozymis'] == 1): ?>
            <div class="deal-title top">Top automobilis</div>
        <?php endif; ?>

        <?php if($car['pozymis'] == 2): ?>
            <div class="deal-title">Kiti automobiliai</div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if($gallery){
        $mainPhotos = '';
        $thumbPhotos = '';
        $bigPhotos = '';

        foreach ($gallery as $photo) {
            $imgFile = $photo["photo"];

            $options = array(
                'type' => 'center',
                'width' => 221,
                'height' => 183,
                'quality' => 80,
                'forced' => false
            );
            $imgSmall = ipReflection($imgFile, $options);

            $options2 = array(
                'type' => 'center',
                'width' => 760,
                'height' => 570,
                'quality' => 80,
                'forced' => true
            );
            $imgBig = ipReflection($imgFile, $options2);

            $options3 = array(
                'type' => 'fit',
                'width' => 1920,
                'height' => 1080,
                'quality' => 80,
                'forced' => false
            );
            $imgXBig = ipReflection($imgFile, $options3);


            $image = ipFileUrl($imgSmall);
            $imageBig = ipFileUrl($imgBig);
            $imageXBig = ipFileUrl($imgXBig);

            $mainPhotos .= '<a href="'.$imageXBig.'" target="_blank"><img alt="" src="'.$imageBig.'"> <img class="zoom" src="'.ipThemeUrl("assets/img/icon_zoom.png") .'" alt="Zoom"></a>';
            $thumbPhotos .= '<div><img alt=""src="'.$image.'"></div>';
        }
    } ?>
    <div class="slider_main"><?php echo $mainPhotos ?></div>
    <div class="slider_thumb"><?php echo $thumbPhotos ?></div>
</div>

<div class="col-md-6">
    <div class="price">Kaina: <?php echo $car['kaina']; ?> €</div>
    <table class="info">
        <tbody>
            <?php if($car['metai']): ?>
                <tr>
                    <td>Pagaminimo data:</td>
                    <td><?php echo $car['metai']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['rida']): ?>
                <tr>
                    <td>Rida:</td>
                    <td><?php echo $car['rida']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['kuro_tipas']): ?>
                <tr>
                    <td>Kuro tipas:</td>
                    <td><?php echo $car['kuro_tipas']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['duru_skaicius']): ?>
                <tr>
                    <td>Durų skaičius:</td>
                    <td><?php echo $car['duru_skaicius']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['pavaru_deze']): ?>
                <tr>
                    <td>Pavarų dėžės tipas:</td>
                    <td><?php echo $car['pavaru_deze']; ?></td>
                </tr>
            <?php endif; ?>

            <?php if($car['spalva']): ?>
                <tr>
                    <td>Spalva:</td>
                    <td><?php echo $car['spalva']; ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>

    <?php if($car['kontaktai_vardas']): ?>
        <div class="contacts">
           <?php if($car['kontaktai_foto']): ?>
                <?php
                    $imgFile = $car["kontaktai_foto"];
                    $options = array(
                        'type' => 'center',
                        'width' => 150,
                        'height' => 150,
                        'quality' => 95,
                        'forced' => false
                    );

                    $imgUrl = ipReflection($imgFile, $options);
                ?>
                <img src="<?php echo ipFileUrl($imgUrl); ?>">
            <?php endif; ?>
            <ul>
                <li><strong><?php echo $car['kontaktai_vardas']; ?></strong></li>
                <li><?php echo $car['kontaktai_pareigos']; ?></li>
                <li><a href="tel:<?php echo $car['kontaktai_tel']; ?>"><?php echo $car['kontaktai_tel']; ?></a></li>
                <li><a href="mailto:<?php echo $car['kontaktai_email']; ?>"><?php echo $car['kontaktai_email']; ?></a></li>
            </ul>
        </div>
    <?php endif; ?>

    <?php echo $form->render(); ?>
</div>

<div class="col-md-12">
    <a href="#" class="close_button"><span class="arrow">←</span>   Atgal į pasiūlymus</a>
    <div class="description">
        <?php echo $car['aprasymas']; ?>
    </div>
</div>

<?php ipSetLayout('klasera-full.php'); ?>
<h1 class="car-name"><?php echo $product['pavadinimas']; ?></h1>

<div class="col_6 cars-slider">
    <div class="slick-slider">
        <?php foreach($product['gallery'] as $i => $image) { ?>
            <div>
                <img src="<?php echo $image['photo']; ?>" alt="<?php echo 'photo-'.$i; ?>" />
            </div>
        <?php } ?>
    </div>
    <!--<img src="<?php //echo $product['nuotrauka']; ?>" alt="<?php //echo $product['pavadinimas']; ?>" /> -->

    <div class="slick-slider-pager">
        <?php foreach($product['gallery_thums'] as $i => $image) { ?>
            <div>
                <img src="<?php echo $image['photo']; ?>" alt="<?php echo 'thumb-'.$i; ?>" />
            </div>
        <?php } ?>
    </div>
</div>
<div class="col_6 car-right">

    <span class="car-right-price">Kaina: <?php echo $product['kaina']; ?> €</span><br>
    <div class="info-wrap">
        <div class="type-wrap">
            <?php if($product['metai']) { echo '<span class="right-type">Pagaminimo data: </span><br>'; } ?>
            <?php if($product['rida']) { echo '<span class="right-type">Rida: </span><br>'; } ?>
            <?php if($product['kuro_tipas']) { echo '<span class="right-type">Kuro tipas: </span><br>'; } ?>
            <?php if($product['duru_skaicius']) { echo '<span class="right-type">Durų skaičius: </span><br>'; } ?>
            <?php if($product['pavaru_deze']) { echo '<span class="right-type">Pavarų dėžės tipas: </span><br>'; } ?>
            <?php if($product['spalva']) { echo '<span class="right-type">Spalva: </span>'; } ?>
        </div>
        <div class="value-wrap">
            <?php if($product['metai']) { echo '<span class="right-value">'.$product['metai'].'</span><br>'; } ?>
            <?php if($product['rida']) { echo '<span class="right-value">'.$product['rida'].'</span><br>'; } ?>
            <?php if($product['kuro_tipas']) { echo '<span class="right-value">'.$product['kuro_tipas'].'</span><br>'; } ?>
            <?php if($product['duru_skaicius']) { echo '<span class="right-value">'.$product['duru_skaicius'].'</span><br>'; } ?>
            <?php if($product['pavaru_deze']) { echo '<span class="right-value">'.$product['pavaru_deze'].'</span><br>'; } ?>
            <?php if($product['spalva']) { echo '<span class="right-value">'.$product['spalva'].'</span><br>'; } ?>
        </div>
    </div>
    <?php if($product['kontaktai']) {
            echo '<span class="right-type">Kontaktai:</span> ';
            echo '<span class="right-type">'.$product['kontaktai'].'</span>';
    } ?>
</div>

<div class="col_12">
    <div class="product-description">
        <?php echo $product['aprasymas']; ?>
    </div>

        <a href="https://www.klasera.lt/lt/klasera/prekyba-naudotais-automobiliais" style="margin-top:20px; display:block;">&lt;&lt; Atgal į automobilių sąrašą</a>
</div>


<?php 
	
    $current_language = ipContent()->getCurrentLanguage();
    $current_ln_code = $current_language->getCode();
	$default_site_url = ipConfig()->baseUrl();
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	$heading = false;
	
	if (strpos($url,'galerija') === false) {
	$output = '<div id="ipBlock-main" data-revisionid="79" data-languageid="0" class="ipBlock">
				   <div class="ipWidget ipWidget-Heading ipSkin-default">
						<h1 class="_title">
							'.$item['title_'.$current_ln_code].'   
						</h1>
					</div>';
	$output .= $item['text_'.$current_ln_code];
	}
	
// ============================================================= technika =======================================================================
	
	if (strpos($url,'technika') !== false) {
		$where2 = array('Enabled'=>'1');
		$list2 = ipDb()->selectAll('lagrilita_produktai','*',$where2,' ORDER BY `produktaiOrder` ');
		foreach ($list2 as $item2){
			if ($item2['technika'] != ''){
				if ($heading == false){
					$output .= '<h2>Panaudojimas:</h2>';
					$output .= '<ul>';
					$heading = true;
				}
				if(strpos($item2['technika'],'"'.$item['id'].'"') !== false){
					if ($item2['text_'.$current_ln_code] != ''){
						$output .= '<li><a href="'.$default_site_url.'/produktai/'.$item2['id'].'">'.$item2['title_'.$current_ln_code].'</a></li>';
					}else{
						$output .= '<li>'.$item2['title_'.$current_ln_code].'</li>';
					}
				}
			}                                                 
		}
		if ($heading == true){
			$output .= '</ul>';
			$heading = false;
		}
		
		if ($item['galerijos'] != ''){
			$where2 = array('Enabled'=>'1');
			$list2 = ipDb()->selectAll('lagrilita_galerijos','*',$where2,' ORDER BY `galerijosOrder` ');
			$list3 = ipDb()->selectAll('lagrilita_nuotraukos','*',$where2,' ORDER BY `nuotraukosOrder` ');
			foreach ($list2 as $item2){
				if(strpos($item['galerijos'],'"'.$item2['id'].'"') !== false){
					if ($list3){
						foreach ($list3 as $item3){
							if($item2['id'] == $item3['galerija']){
								$empty = false;
							}else{
								$empty = true;
							}
						}						
						if ($empty === false){
							if ($heading == false){
								$output .= '<h2>Galerijos:</h2>';
								$output .= '<ul>';
								$heading = true;
							}
							$output .= '<li><a href="'.$default_site_url.'/galerijos/'.$item2['id'].'">'.$item2['title_'.$current_ln_code].'</a></li>';
						}
					}
				}
			}
			if ($heading == true){
				$output .= '</ul>';
				$heading = false;
			}
		}

// ============================================================= statyba =======================================================================
		
	} else if ((strpos($url,'statyba') !== false) || (strpos($url,'aplinka') !== false) || (strpos($url,'kita') !== false)){
		
		if ($item['galerijos'] != ''){
			$where2 = array('Enabled'=>'1');
			$list2 = ipDb()->selectAll('lagrilita_galerijos','*',$where2,' ORDER BY `galerijosOrder` ');
			$list3 = ipDb()->selectAll('lagrilita_nuotraukos','*',$where2,' ORDER BY `nuotraukosOrder` ');
			foreach ($list2 as $item2){
				if(strpos($item['galerijos'],'"'.$item2['id'].'"') !== false){
					if ($list3){
						foreach ($list3 as $item3){
							if($item2['id'] == $item3['galerija']){
								$empty = false;
							}else{
								$empty = true;
							}
						}
						if ($empty === false){
							if ($heading == false){
								$output .= '<h2>Galerijos:</h2>';
								$output .= '<ul>';
								$heading = true;
							}
							$output .= '<li><a href="'.$default_site_url.'/galerijos/'.$item2['id'].'">'.$item2['title_'.$current_ln_code].'</a></li>';
						}							
					}
				}
			}
			if ($heading == true){
				$output .= '</ul>';
				$heading = false;
			}
		}
		
		if ($item['technika'] != ''){
			$where2 = array('Enabled'=>'1');
			$list2 = ipDb()->selectAll('lagrilita_technika','*',$where2,' ORDER BY `technikaOrder` ');
			if ($list2){
				foreach ($list2 as $item2){
					if (strpos($item['technika'],'"'.$item2['id'].'"') !== false){
						if ($heading == false){
							$output .= '<h2>Naudojama technika</h2>';
							$output .= '<ul>';
							$heading = true;
						}
						if ($item2['text_'.$current_ln_code] != ''){
							$output .= '<li><a href="/technika/'.$item2['id'].'/">'.$item2['title_'.$current_ln_code].'</a></li>';
						}else{
							$output .= '<li>'.$item2['title_'.$current_ln_code].'</li>';
						}
					}
				}
				if ($heading == true){
					$output .= '</ul>';
					$heading = false;
				}
			}
		}
// ============================================================= galerija =======================================================================
	} else if (strpos($url,'galerija') !== false)
	
	$output .= '<div class="my_clear"><!-- --></div>';
	// back button
	
	$output .= '<div id="goBack"><a class="goBack">';
	if ($current_ln_code ==	'lt'){
		$output .= 'Atgal';
	}else if ($current_ln_code == 'ru'){
		$output .= 'Назад';
	}
	$output .= '</a>';
	
    echo $output;

?>
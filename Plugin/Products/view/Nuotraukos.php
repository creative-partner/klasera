<?php 
	
    $current_language = ipContent()->getCurrentLanguage();
    $current_ln_code = $current_language->getCode();
	$default_site_url = ipConfig()->baseUrl();
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	$heading = false;
	
	$output = '<h1>'.$item['title_'.$current_ln_code].'</h1>';
	$output .= $item['text_'.$current_ln_code];
	$output .= '<div id="galerijos">';
	$where2 = array('Enabled'=>'1');
	$list2 = ipDb()->selectAll('lagrilita_galerijos','*',$where2,' ORDER BY `galerijosOrder` ');
	$list3 = ipDb()->selectAll('lagrilita_nuotraukos','*',$where2,' ORDER BY `nuotraukosOrder` ');
		foreach ($list3 as $item3){
			if (($item3['galerija'] == $item['id'])){
				$output .= '<div class="col col-xs-12 col-sm-5 col-md-4 col-lg-4">';
				$output .= '<a href="/file/repository/lagrilita/images/'.$item3['nuotrauka'].'" class="nuotrauka">';
				$output .= '<img src="/file/repository/lagrilita/images/'.$item3['nuotrauka'].'">';
				$output .= '<h4>'.$item3['title_'.$current_ln_code].'</h4>';
				$output .= '</a></div>';
			}
		}
	$output .= '</div>';
	$output .= '<div class="my_clear"><!-- --></div>';
	// back button
	
	$output .= '<div id="goBack"><a class="goBack">';
	if ($current_ln_code ==	'lt'){
		$output .= 'Atgal';
	}
	$output .= '</a>';
	
    echo $output;

?>
<?php //print_r($cars); ?>

<?php foreach ($cars as $car): ?>
    <div class="col-md-6 item">
        <?php
            $options = array(
                'type' => 'center',
                'width' => 930,
                'height' => 470,
                'quality' => 95,
            );

            $thumbnail = ipReflection($car['nuotrauka'], $options);
        ?>
        <?php if($car['pozymis'] != 0): ?>
                <?php if($car['pozymis'] == 1): ?>
                    <div class="deal-title top">Top automobilis</div>
                <?php endif; ?>

                <?php if($car['pozymis'] == 2): ?>
                    <div class="deal-title">Kiti automobiliai</div>
                <?php endif; ?>
        <?php endif; ?>
        <div class="photo">
            <a href="<?php echo ipRouteUrl('Products.singleCarDeal', array('url' => $car['url'],'id' => $car['id'])); ?>">
                <img src="<?php echo ipFileUrl($thumbnail); ?>" alt="">
            </a>
        </div>
        <div class="desc">
            <h2><?php echo $car['pavadinimas'] ?></h2>
            <h3><?php echo $car['kaina'] . ' €' ?></h3>
            <div class="info">
                <ul>
                    <?php if($car['metai']): ?><li><?php echo $car['metai']; ?></li><?php endif; ?>
                    <?php if($car['kuro_tipas']): ?><li><?php echo $car['kuro_tipas']; ?></li><?php endif; ?>
                    <?php if($car['pavaru_deze']): ?><li><?php echo $car['pavaru_deze']; ?></li><?php endif; ?>
                    <?php if($car['galingumas']): ?><li><?php echo $car['galingumas']. ' kW'; ?></li><?php endif; ?>
                    <?php if($car['duru_skaicius']): ?><li><?php echo $car['duru_skaicius']; ?></li><?php endif; ?>
                    <?php if($car['rida']): ?><li><?php echo $car['rida']. ' km'; ?></li><?php endif; ?>
                    <?php if($car['spalva']): ?><li><?php echo $car['spalva']; ?></li><?php endif; ?>
                </ul>
                <?php if($car['kontaktai_vardas']): ?>
                    <div class="contacts">
                        <?php if($car['kontaktai_foto']): ?>
                            <?php
                            $imgFile = $car["kontaktai_foto"];
                            $options = array(
                                'type' => 'center',
                                'width' => 150,
                                'height' => 150,
                                'quality' => 95,
                                'forced' => false
                            );

                            $imgUrl = ipReflection($imgFile, $options);
                            ?>
                            <img src="<?php echo ipFileUrl($imgUrl); ?>">
                        <?php endif; ?>
                        <ul>
                            <li><strong><?php echo $car['kontaktai_vardas']; ?></strong></li>
                            <li><?php echo $car['kontaktai_pareigos']; ?></li>
                            <li><a href="tel:<?php echo $car['kontaktai_tel']; ?>"><?php echo $car['kontaktai_tel']; ?></a></li>
                            <li><a href="mailto:<?php echo $car['kontaktai_email']; ?>"><?php echo $car['kontaktai_email']; ?></a></li>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>

 $('.slick-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slick-slider-pager').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slick-slider',
  dots: false,
  arrows:true,
  centerMode: false,
  focusOnSelect: true
}); 

$("#all-cars .car-item:nth-child(3n)").css("border-right", "none");


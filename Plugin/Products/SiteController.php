<?php
namespace Plugin\Products;

class SiteController extends \Ip\Controller
{

    public static function generateForm(){
        $form = new \Ip\Form();
        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'name',
                'label' => 'Vardas, pavardė:',
                'validators' => array('Required')
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Email(
            array(
                'name' => 'email',
                'label' => 'El. paštas:',
                'validators' => array('Required')
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'name' => 'phone',
                'label' => 'Telefono numeris:',
                'validators' => array('Required')
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'sa',
                'value' => 'Products.saveForm',
            ));
        $form->addField($field);

        $field = new \Ip\Form\Field\Submit(
            array(
                'value' => 'Siųsti'
            )
        );
        $form->addField($field);

        return $form;
    }

    public static function saveForm(){
        $form =  self::generateForm();
        $postData = ipRequest()->getPost();
        $errors = $form->validate($postData);

        if ($errors) {
            // Validation error
            $status = array('status' => 'error', 'errors' => $errors);
            return new \Ip\Response\Json($status);
        } else {
            // Success
            //$senderEmail = ipGetOptionLang('Config.websiteEmail');

            $senderEmail = ipGetOptionLang('Config.websiteEmail');
            $name = ipRequest()->getPost('name');
            $email = ipRequest()->getPost('email');
            $phone = ipRequest()->getPost('phone');

            $mail = "Gaukite pasiūlymą<br>
                Vardas ir pavardė". $name ."<br>
                Telefono numeris: " . $phone . "<br>
                El. paštas: ". $email . "<br>
                ";

            ipSendEmail($senderEmail, "Klasera", "test123@test.lt", "Bosch", "Paslaugos užsakymas", $mail);

            $actionUrl = ipActionUrl(array('sa' => 'Products.formSuccess'));
            $status = array('redirectUrl' => $actionUrl);
            return new \Ip\Response\Json($status);
        }
    }

    public static function formSuccess()
    {
        $renderedHtml = ipView('view/success.php')->render();
        return $renderedHtml;
    }


}

<?php
    namespace Plugin\Products;

    class Event {
        public static function ipBeforeController() {
            ipAddCss('assets/slick-slider/slick.css');
            ipAddCss('assets/slick-slider/slick-theme.css');
            ipAddCss('assets/style.css');
            ipAddCss('assets/responsive.css');
            ipAddJs('assets/slick-slider/slick.js');
            ipAddJs('assets/script.js');
        }
    }
?>
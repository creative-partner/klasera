<?php
namespace Plugin\Products\Widget\Products;

use \Plugin\Products\Model;
class Controller extends \Ip\WidgetController
{
    public function generateHtml($revisionId,$widgetId,$data,$skin)
    {
        $data['data'] = Model::getAllCars();
        return parent::generateHtml($revisionId,$widgetId,$data,$skin);
    }
}

<div id="all-cars">
    <div class="row">
	<?php
	foreach ($data as $key => $item) { ?>

        <div class="col-lg-4 car-item">
            <div class="car-image">
                <img src="<?php echo $item['nuotrauka'] ?>" alt="image" />
            </div>
            <div class="car-info">
                <a href="<?php echo ipRouteUrl('Products.products', array('url' => $item['url'],'id' => $item['id'])); ?>"><?php echo $item['pavadinimas']; ?></a>
                <span class="car-price"><?php echo $item['kaina']. ' €'; ?></span><br>
                <span><?php echo $item['metai']; ?></span>
                <span><?php echo $item['kuro_tipas']; ?></span>
                <span><?php echo $item['pavaru_deze']; ?></span>
                <span><?php echo $item['galingumas']. ' kW'; ?></span>
                <span><?php echo $item['duru_skaicius']; ?></span>
                <span><?php echo $item['rida']. ' km'; ?></span>
                <span><?php echo $item['spalva']; ?></span>
            </div>
        </div>
	<?php }	?>
    </div>
</div>
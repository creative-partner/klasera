<?php

namespace Plugin\Products;

class PublicController extends \Ip\Controller {
	public static function getProduct($url,$id) {
        $product = Model::getProduct($url,$id);

        return ipView('view/Produktas.php', array('product' => $product));
    }

    public static function singleCarDeals($url, $id){
	    $car = Model::getSingleCarDeals($id);
	    $gallery = Model::getProductGallery($id);
	    $form = SiteController::generateForm();

        return ipView('view/singleDeals.php', array('car' => $car, 'gallery' => $gallery, 'form' => $form));
    }

}

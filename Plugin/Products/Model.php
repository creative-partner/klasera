<?php
namespace Plugin\Products;

class Model {
    public static function getAllCars() {
        $data = ipDb()->selectAll('products', '*');
        $data = array_map( function($item) {
            $item['nuotrauka'] = ipReflection($item['nuotrauka'], array('type' => 'center', 'width' => 530, 'height' => 400));
            $item['nuotrauka'] = ipFileUrl($item['nuotrauka']);
            return $item;
        }, $data );
        return $data;
    }

    public static function getProductGallery($id) {
        return ipDb()->selectAll('galerija','*',array('car_id' => $id),'ORDER BY `gallery_order` ASC');
    }

    public static function getProduct($url,$id) {
        $product = ipDb()->selectRow('products','*',array('id' => $id));

        //gallery photos
        $product['gallery'] = self::getProductGallery($id);
        $product['gallery'] = array_map(function($item) {
            $item['photo'] = ipReflection($item['photo'], array('type' => 'center', 'width' => 530, 'height' => 400, 'quality' => 90));

        $item['photo'] = ipFileUrl($item['photo']);
        return $item;
        }, $product['gallery']);

        //gallery thumbs

        $product['gallery_thums'] = self::getProductGallery($id);
        $product['gallery_thums'] = array_map(function($item) {
            $item['photo'] = ipReflection($item['photo'], array('type' => 'center', 'width' => 110, 'height' => 80, 'quality' => 90));

        $item['photo'] = ipFileUrl($item['photo']);
        return $item;
        }, $product['gallery_thums']);

        $product['nuotrauka'] = ipReflection($product['nuotrauka'], array('type' => 'center','width' => 530, 'height' => 400, 'quality' => 90));

        $product['nuotrauka'] = ipFileUrl($product['nuotrauka']);

        return $product;
    }

    public static function getAllCarDeals(){
        $table = ipTable('products');

        $sql = "SELECT * FROM $table WHERE `stendas`=1 ORDER BY `productsOrder`";

        $items = ipDb()->fetchAll($sql);

        return $items;
    }

    public static function getSingleCarDeals($id){
        $table = ipTable('products');

        $sql = "SELECT * FROM $table WHERE stendas=1 AND `id`=$id";

        $item = ipDb()->fetchRow($sql);

        return $item;
    }
}

?>

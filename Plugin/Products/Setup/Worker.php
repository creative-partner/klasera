<?php
namespace Plugin\Products\Setup;
class Worker extends \Ip\SetupWorker{

	public function activate(){
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/file/repository/Products/images/categories')) {
                mkdir($_SERVER['DOCUMENT_ROOT'].'/file/repository/Products/images/categories', 0755, true);
        }

		$sql = '
			CREATE TABLE IF NOT EXISTS
			   ' . ipTable('products') . '
			(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`products-order` double,
			`pavadinimas` varchar(255),
			`kaina` varchar(255),
			`metai` varchar(255),
			`kuro_tipas` varchar(255),
			`pavaru_deze` varchar(255),
			`galingumas` varchar(255),
			`duru_skaicius` varchar(255),
			`rida` varchar(255),
			`spalva` varchar(255),
			`nuotrauka` varchar(255),
			`galerija` varchar(255),
			`aprasymas` text,
			`kontaktai_foto` varchar(255),
			`kontaktai_vardas` varchar(255),
			`kontaktai_pareigos` varchar(255),
			`kontaktai_tel` varchar(255),
			`kontaktai_email` varchar(255),
			`stendas` boolean,
			PRIMARY KEY (`id`)
			)';
		ipDb()->execute($sql);

		$sql = '
			CREATE TABLE IF NOT EXISTS
			   ' . ipTable('galerija') . '
			(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`photo` varchar(255),
            `gallery_order` double,
            `car_id` varchar(255),
			PRIMARY KEY (`id`)
			)';
		ipDb()->execute($sql);

	}

	public function deactivate()
    {
		// ...........
    }
    public function remove()
    {
		// ...........
        $sql = '
			DROP TABLE IF EXISTS
			   ' . ipTable('products');
        ipDb()->execute($sql);
    }
}

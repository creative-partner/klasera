<?php

namespace Plugin\SMTPConfig;


class Filter
{
    public static function ipSendEmailPHPMailerObject(\PHPMailer $mail)
    {
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = 'smtp.sendgrid.net'; // SMTP server
        //$mail->SMTPDebug = 2; // enables SMTP debug information (for testing)
        $mail->SMTPAuth = ipGetOption('SMTPConfig.smtpAuth', true); // enable SMTP authentication
        $mail->Port = 587; // set the SMTP port for the GMAIL server
        $mail->Username = ipGetOption('SMTPConfig.username'); // SMTP account username
        $mail->Password = ipGetOption('SMTPConfig.password');
        $mail->SMTPSecure = 'TLS';
        return $mail;
    }
}

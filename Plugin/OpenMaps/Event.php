<?php

namespace Plugin\OpenMaps;


class Event
{
    public static function ipBeforeController()
    {
        ipAddCss('https://unpkg.com/leaflet@1.3.4/dist/leaflet.css');
        ipAddJs('https://unpkg.com/leaflet@1.3.4/dist/leaflet.js');
    }
}

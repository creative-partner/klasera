<?php

namespace Plugin\OpenMaps;


class AdminController
{

    /**
     * WidgetSkeleton.js ask to provide widget management popup HTML. This controller does this.
     * @return \Ip\Response\Json
     * @throws \Ip\Exception\View
     */
    public function widgetPopupHtml()
    {
        $widgetId = ipRequest()->getQuery('widgetId');
        $widgetRecord = \Ip\Internal\Content\Model::getWidgetRecord($widgetId);
        $widgetData = $widgetRecord['data'];

        //create form prepopulated with current widget data
        $form = $this->managementForm($widgetData);

        //Render form and popup HTML
        $viewData = array(
            'form' => $form
        );
        $popupHtml = ipView('view/editPopup.php', $viewData)->render();
        $data = array(
            'popup' => $popupHtml
        );
        //Return rendered widget management popup HTML in JSON format
        return new \Ip\Response\Json($data);
    }


    /**
     * Check widget's posted data and return data to be stored or errors to be displayed
     */
    public function checkForm()
    {
        $data = ipRequest()->getPost();
        $form = $this->managementForm();
        $data = $form->filterValues($data); //filter post data to remove any non form specific items
        $errors = $form->validate($data); //http://www.impresspages.org/docs/form-validation-in-php-3
        if ($errors) {
            //error
            $data = array (
                'status' => 'error',
                'errors' => $errors
            );
        } else {
            //success
            unset($data['aa']);
            unset($data['securityToken']);
            unset($data['antispam']);
            $data = array (
                'status' => 'ok',
                'data' => $data

            );
        }
        return new \Ip\Response\Json($data);
    }

    protected function managementForm($widgetData = array())
    {
        $form = new \Ip\Form();

        $form->setEnvironment(\Ip\Form::ENVIRONMENT_ADMIN);

        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'aa',
                'value' => 'OpenMaps.checkForm'
            )
        );
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'label' => 'Latitude',
                'name' => 'lat',
                'value' => empty($widgetData['lat']) ? null : $widgetData['lat']
            )
        );
        $field->addValidator('Required');
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'label' => 'Longitude',
                'name' => 'lng',
                'value' => empty($widgetData['lng']) ? null : $widgetData['lng']
            )
        );
        $field->addValidator('Required');
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'label' => 'Icon',
                'name' => 'icon',
                'value' => empty($widgetData['icon']) ? null : $widgetData['icon']
            )
        );
        $form->addField($field);

        $field = new \Ip\Form\Field\Text(
            array(
                'label' => 'Icon size (width, height)',
                'name' => 'icon_size',
                'value' => empty($widgetData['icon_size']) ? null : $widgetData['icon_size']
            )
        );
        $form->addField($field);

        $field = new \Ip\Form\Field\Integer(
            array(
                'label' => 'Zoom',
                'name' => 'zoom',
                'defaultValue' => 12,
                'value' => empty($widgetData['zoom']) ? 12 : $widgetData['zoom']
            )
        );
        $form->addField($field);

        $field = new \Ip\Form\Field\Hidden(
            array(
                'name' => 'uniqueId',
                'defaultValue' => rand(1,99),
                'value' => empty($widgetData['uniqueId']) ? rand(1,99) : $widgetData['uniqueId']
            )
        );
        $form->addField($field);

        return $form;
    }
}

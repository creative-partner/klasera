<?php if(!empty($lat) && !empty($lat) && !empty($zoom) && !empty($uniqueId)): ?>
    <div id="map-<?php echo $uniqueId ?>" style="width:100%; height: 450px; z-index: 2;"></div>

	<script>
        window.addEventListener("load",function(event) {
            if (document.getElementById("map-<?php echo $uniqueId ?>")) {
                var map = L.map('map-<?php echo $uniqueId ?>').setView([<?php echo $lat ?>, <?php echo $lng ?>], <?php echo $zoom ?>);

                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: ''
                }).addTo(map);

                var LeafIcon = L.Icon.extend({
                    options: {
                        iconSize:     [<?php echo $icon_size ?>],
                    }
                });

                var greenIcon = new LeafIcon({iconUrl: '<?php echo ipThemeUrl($icon); ?>' });

                L.marker([<?php echo $lat ?>, <?php echo $lng ?>], {icon: greenIcon}).addTo(map);
            }
        },false);
	</script>
<?php endif; ?>

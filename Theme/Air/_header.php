<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <?php
          ipAddCss('assets/theme.css');
		  ipAddCss('styles/style.css');
          ipAddCss('styles/responsive.css');
    ?>
	<link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <?php echo ipHead(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:image" content="<?php echo ipThemeUrl('assets/img/klasera-logo2.png'); ?>" />
</head>
<body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?>">
<div class="wrapper clearfix">
    <?php
    echo ipBlock('bosch')->asStatic()->render();
    ?>
    <header class="clearfix col_12">
        <div class="header-wrapp">
            <div class="logo">
                <a href="https://www.klasera.lt/lt/klasera">
                    <img src="<?php echo ipThemeUrl('images/klasera-logo.png') ?>" alt="klasera_logo" />
                </a>
            </div>
            <div class="left">
                <span class="currentPage"><?php echo esc(ipContent()->getCurrentPage() ? ipContent()->getCurrentPage()->getTitle() : ''); ?></span>
                <a href="#" class="topmenuToggle">&nbsp;</a>
                <a class="home-link" href="<?php echo ipHomeUrl(); ?>"></a>
                <div class="topmenu">
                    <?php echo ipSlot('menu', 'klasera-menu'); ?>
                    <?php if (count(ipContent()->getLanguages()) > 1) { ?>
                        <div class="languages">
                            <?php echo ipSlot('languages'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div>
            </div>
        </div>
    </header>

<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
    <head>
        <link rel="stylesheet" href="<?php echo ipThemeUrl('styles/deals.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo ipThemeUrl('assets/css/slick.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo ipThemeUrl('assets/css/slick-theme.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo ipThemeUrl('assets/css/slick-lightbox.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo ipThemeUrl('assets/css/keyboard.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo ipThemeUrl('assets/css/keyboard-basic.min.css') ?>">
        <?php echo ipHead(); ?>
        <link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
    </head>
    <body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?>">
        <div class="container deal">
            <div class="row">
                <?php echo ipBlock('main')->render(); ?>
            </div>
        </div>

        <?php echo ipJs(); ?>
        <script src="<?php echo ipThemeUrl('assets/js/slick.min.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('assets/js/slick-lightbox.min.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('assets/js/jquery.keyboard.min.js') ?>"></script>
        <script src="<?php echo ipThemeUrl('assets/js/ms-Lithuanian.js') ?>"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery(".get_offer").click(function (e) {
                    e.preventDefault();
                    jQuery('.contact-form').fadeIn();
                });

                jQuery('.close-form').click(function(){
                    jQuery('.contact-form').fadeOut();
                    jQuery(".contact-form input:not([type=hidden])").val("");
                });

                jQuery('.slider_main').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: false,
                    asNavFor: '.slider_thumb'
                });

                jQuery('.slider_thumb').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.slider_main',
                    arrows: false,
                    dots: false,
                    centerMode: true,
                    focusOnSelect: true,
                });

                jQuery('.slider_main').slickLightbox({
                    src: 'href',
                    itemSelector: 'a',
                    imageMaxHeight: 1
                });

                jQuery.keyboard.keyaction.enter = function( kb ) {
                    kb.close( true );
                    return false;     // return false prevents further processing
                };

                jQuery('input').keyboard({
                    layout: 'ms-Lithuanian',
                    usePreview: false,
                    autoAccept : true,
                    noFocus : false,
                    position:false,
                    css: {
                        buttonHover: 'button_hover'
                    }
                });

                mainBack();
            });

            function mainBack() {
                var t;
                window.onload = resetTimer;
                window.onmousemove = resetTimer;
                window.onmousedown = resetTimer;  // catches touchscreen presses as well
                window.ontouchstart = resetTimer; // catches touchscreen swipes as well
                window.onclick = resetTimer;      // catches touchpad clicks as well
                window.onkeypress = resetTimer;
                window.addEventListener('scroll', resetTimer, true); // improved; see comments

                function redirectMain() {
                    window.location = '/lt/info-stendas';
                }

                function resetTimer() {
                    clearTimeout(t);
                    t = setTimeout(redirectMain, 600000);  // time is in milliseconds
                }
            }
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-71698335-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>

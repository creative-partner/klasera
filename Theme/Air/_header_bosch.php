<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
        <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <?php ipAddCss('assets/theme.css');
		  ipAddCss('styles/style.css');
		  ipAddCss('styles/responsive.css');?>
    <?php echo ipHead(); ?>
    <link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="<?php echo ipThemeUrl('assets/img/bosch-logo.png'); ?>" />
</head>
<body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?> bosch-page">
<div class="wrapper bosh-wrapp clearfix">
    <?php
    echo ipBlock('klasera')->asStatic()->render();
    ?>
    <header class="clearfix col_12">
        <div class="bosh-header-wrapp">
            <?php echo ipBlock('bosch-logo-big')->asStatic()->render(); ?>
            <div class="left">
                <span class="currentPage"><?php echo esc(ipContent()->getCurrentPage() ? ipContent()->getCurrentPage()->getTitle() : ''); ?></span>
                <a href="#" class="topmenuToggle">&nbsp;</a>
                <a class="home-link" href="<?php echo ipHomeUrl(); ?>"></a>
                <div class="topmenu">
                    <?php echo ipSlot('menu', 'menu2'); ?>
                    <?php if (count(ipContent()->getLanguages()) > 1) { ?>
                        <div class="languages">
                            <?php echo ipSlot('languages'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div>
            </div>
        </div>

    </header>
    <div class="clear"></div>

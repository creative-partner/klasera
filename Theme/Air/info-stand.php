<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <link rel="stylesheet" href="<?php echo ipThemeUrl('styles/deals.css') ?>">
    <link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
</head>
<body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?>">
    <div class="fullscreen">
        <div class="disable-video">Peržiūrėti pasiūlymus</div>
        <video class="video" loop muted>
            <source src="<?php echo ipThemeUrl('assets/video/11306en_audi-media-tv_8000k.mp4') ?>" type="video/mp4">
        </video>
    </div>

    <div class="container deals">
        <div class="row contacts">
            <div class="col-xs-12">
                <div class="contacts-info">
                    <div class="wrapper">
                        <h3>Klaipėdos Audi centras</h3>
                        <ul>
                            <li>Šilutės pl. 50, Klaipėda, LT-94181,</li>
                            <li>tel.: +370 46 340 124; +370 46 342 230; +370 612 58 526,</li>
                            <li>el. p.: info@klasera.lt</li>
                        </ul>
                    </div>
                    <img src="<?php echo ipThemeUrl('assets/img/audi_logo_stand.png'); ?>" alt="Audi">
                </div>
            </div>
        </div>
        <div class="row">
            <?php echo ipSlot('allCarDeals'); ?>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            //showVideo();
        });

        function showVideo() {
            var t;
            window.onload = resetTimer;
            window.onmousemove = resetTimer;
            window.onmousedown = resetTimer;  // catches touchscreen presses as well
            window.ontouchstart = resetTimer; // catches touchscreen swipes as well
            window.onclick = resetTimer;      // catches touchpad clicks as well
            window.onkeypress = resetTimer;
            window.addEventListener('scroll', resetTimer, true); // improved; see comments

            function startVideo() {
                jQuery('video').get(0).play();
                jQuery('.fullscreen').fadeIn();
            }

            function resetTimer() {
                jQuery('video').get(0).pause();
                jQuery('.fullscreen').fadeOut();
                clearTimeout(t);
                t = setTimeout(startVideo, 120000);  // time is in milliseconds
            }
        }
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-71698335-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>

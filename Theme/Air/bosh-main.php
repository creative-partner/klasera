<?php echo ipView('_header_bosch.php')->render(); ?>
 <div class="col_12 main inside">
     <div class="row">
        <div class="col-md-3 sidenav left" style="padding-right:60px;">
            <div class="title">
                <?php $title = \Ip\Menu\Helper::getMenuItems('menu2', 1, 1); ?>
                <?php echo ipSlot('menu', $title); ?>	
            </div>
            <nav <?php if (ipGetThemeOption('collapseSidebarMenu') == 'yes') { echo ' class="collapse"'; }?>>
                <?php
                    // generate 2 - 7 levels submenu
                    // please note that it is possible to generate second level only if first level item is in breadcrumb

                        //$pages = \Ip\Menu\Helper::getChildItems();
                       // echo ipSlot('menu', $pages);
                    $pages = \Ip\Menu\Helper::getMenuItems('menu2', 2, 7);
                    echo ipSlot('menu', $pages);

                     //submenu of currently active menu item
                     //$pages = \Ip\Menu\Helper::getChildItems();
                     //echo ipSlot('menu', $pages);

                    //echo ipSlot('menu', 'klasera-meniu');
                ?>
            </nav>
        </div>

        <div class="content" style="width:70%;">
            <?php echo ipBlock('main')->render(); ?>
        </div>
    </div>
</div>	
    <div class="clear"></div>
<?php echo ipView('_footer_bosch.php')->render(); ?>

<?php echo ipView('_header_bosch.php')->render(); ?> 
 <div class="main col_12">
     <div class="main-wrapp">

			<?php echo ipBlock('Title')->render(); ?>
            <div class="slider-menu">
			     <?php echo ipSlot('homeSlider');?>
                <div class="menu">
                    <?php //echo ipBlock('BoschMenu')->render(); ?>
                    <div class="menu-col1">
                        <div class="right-blocks-wrapp">
                            <div class="engine-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'engine',
                                'default' => 'Variklis',
                                'class' => 'engine right-blocks',
                                'tag' => 'div'  
                            )); ?> 
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="brakes-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'brakes',
                                'default' => 'Stabdžiai',
                                'class' => 'brakes right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="battery-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'battery',
                                'default' => 'Akumuliatorius',
                                'class' => 'battery right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="check-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'check',
                                'default' => 'Patikra',
                                'class' => 'check right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="lights-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'lights',
                                'default' => 'Šviesos',
                                'class' => 'lights right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="tyres-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'tyres',
                                'default' => 'Padangos',
                                'class' => 'tyres right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="mechanics-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'mechanics',
                                'default' => 'Mechanika',
                                'class' => 'mechanics right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="electrics-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'electrics',
                                'default' => 'El. sistema',
                                'class' => 'electrics right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                        
                        <div class="right-blocks-wrapp">
                            <div class="conditioning-image"></div>
                            <?php echo ipSlot('text', array(
                                'id' => 'condition',
                                'default' => 'Kondicionavimas',
                                'class' => 'condition right-blocks',
                                'tag' => 'div'  
                            )); ?>
                        </div>
                    </div>
                        <?php //echo ipBlock('BoschMenu_col31')->render(); ?>
                </div>
                <div class="clear"></div>
            </div>
                <div class="value-holder">
                    <?php //echo ipBlock('BoschHolder')->render();
                    //echo ipBlock('Bosch-service')->render();?>
                    <div class="inner-holder">
                        <?php echo ipBlock('HolderTitle')->render();?>
                        <div class="values">
                            <div class="col1">
                                <?php echo ipBlock('bosch-value1')->render();
                                echo ipBlock('bosch-value3')->render();?>
                            </div>
                            <div class="col2">
                                <?php echo ipBlock('bosch-value2')->render();
                                echo ipBlock('bosch-value4')->render();?>
                            </div>	
                        </div>
                    </div>
                </div>
			<div class="info-blocks">
				<div class="info-block1">
					<?php //echo ipBlock('block1')->render(); ?>
					<div class="info-block-content">
						<?php echo ipBlock('block1-title')->render();
						 echo ipBlock('block-text1')->render(); ?>
					</div>
				</div>
				<div class="info-block2">
					<?php //echo ipBlock('block2')->render(); ?>
					<div class="info-block-content">
						<?php echo ipBlock('block2-title')->render();
						 echo ipBlock('block-text2')->render(); ?>
					</div>
				</div>
				<div class="info-block3">
					<?php //echo ipBlock('block3')->render(); ?>
					<div class="info-block-content">
						<?php echo ipBlock('block3-title')->render();
						 echo ipBlock('block-text')->render();?>
                        <div class="block3-button">
						  <?php echo ipBlock('block-button')->render(); ?>
                        </div>
					</div>
				</div>
			</div>
			<?php echo ipBlock('footer-link')->render(); ?>
		  
        </div>
    </div>
    <div class="clear"></div>
<?php echo ipView('_footer_bosch.php')->render(); ?>

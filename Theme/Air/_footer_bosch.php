<footer class="clearfix">
    <div class="col_12">
        <div class="header-wrapp">
            <div class="left">
            <?php
                //echo ipBlock('adresas2')->asStatic()->render();
                //echo ipBlock('telefonas2')->asStatic()->render();
                //echo ipBlock('pastas2')->asStatic()->render();
                //echo ipBlock('c')->asStatic()->render();
            ?>
                <div class="row-cols">
                <div class="col-address">
                    <?php
                    echo ipBlock('adresas2')->asStatic()->render();
                    ?>
                </div>

                <div class="col-phone">
                    <?php
                    echo ipBlock('telefonas2')->asStatic()->render();
                    ?>
                </div>

                <div class="col-email">
                    <?php
                    echo ipBlock('pastas')->asStatic()->render();
                    ?>
                </div>

                <div class="col-policy">
                    <?php
                    echo ipBlock('policy2')->asStatic()->render();
                    ?>
                </div>

                <a href="#"><span class="fb-icon"></span></a>

                </div>
            </div>
        </div>
        <div class="right">
            <?php
			echo ipBlock('copyright')->asStatic()->render();?>


			<div class="solution">
                <a href="https://saskaita123.lt/" target="_blank">Sukūrė:</a>
                <a href="https://cpartner.lt" target="_blank"><span class="creative-partner"></span></a>
            </div>

        </div>
    </div>
</footer>

</div>
<?php echo ipView('_footer_cookies.php')->render(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71698335-1', 'auto');
  ga('send', 'pageview');

</script>
<?php echo ipAddJs('assets/site.js'); ?>
<?php echo ipAddJs('assets/scripts.js'); ?>
<?php echo ipJs(); ?>
<script>
    $(document).ready(function(){
        $(".privacy").colorbox({
            width:"60%",
            className: 'privacy-form',
            html:`<?php echo ipBlock('privacyText')->asStatic()->render(); ?> <a href="#" onclick="privacyAgree()" class="btn btn-default privacy-agree" >Sutinku ir susipažinau</a>`,
        });
    });

    function privacyAgree(){
        $(".checkbox input[name='privacy']").prop('checked', true);
        parent.$.fn.colorbox.close();
        return false;
    }
</script>
<script>
    jQuery( document ).ready( function( $ ) {
        /*        if(getCookie('analytic_cookies')){
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', 'G-F3YSGMXYPF');
                }*/
        jQuery( "#open-settings" ).click(function(e) {
            jQuery('#cookie-primary').removeClass('cookie-agreement-open');
            jQuery('#cookie-agreement').toggleClass('cookie-agreement-open');
            e.preventDefault();
        });
        jQuery( ".cookie-floater" ).click(function(e) {
            e.preventDefault();
            jQuery('#cookie-agreement').toggleClass('cookie-agreement-open');
            jQuery('#cookie-overlay').toggleClass('cookie-agreement-open');
            e.preventDefault();
        });
    });
</script>


</body>
</html>

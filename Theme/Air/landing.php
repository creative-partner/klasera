<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <?php ipAddCss('assets/theme.css');
		  ipAddCss('styles/style.css');
		  ipAddCss('styles/responsive.css');?>
	<link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
    <?php echo ipHead(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php echo ipThemeUrl('assets/img/klasera-logo2.png'); ?>" />
    <meta property="og:url" content="https://www.klasera.lt" />
    <meta property="og:description" content="UAB „Klasera“, kuriai šiuo metų priklauso „Bosch“ automobilių servisas ir Klaipėdos audi centras, adresu Šilutės pl. 50, Klaipėda.
UAB „Baltic Auto“, kuri vykdo Volkswagen automobilių prekybą, techninį aptarnavimą ir remontą, adresu Mokyklos g. 45, Klaipėda.
UAB „Kemsas“ vykdanti automobilių stovėjimo aikštelių veiklą." />
</head>
<body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?>">
    <div class="wrapper clearfix" style="margin-top:30px;">
        <div class="landing-top">
            <div class="landing-logo">
                <a href="#"><img src="<?php echo ipThemeUrl('images/klasera-logo.png'); ?>" alt="logo" /></a>
            </div>
            <div class="landing-address">
                <?php echo ipSlot('text', array('id' => 'top-address', 'tag' => 'span', 'default' => ('Šilutės pl. 50 C, Klaipėda'), 'class' => 'top-address')); ?>
            </div>
            <div class="landing-tel">
                <?php echo ipSlot('text', array('id' => 'top-tel', 'tag' => 'span', 'default' => ('+370 46 34 17 35, +370 698 54 984'), 'class' => 'top-tel')); ?>
            </div>
            <div class="landing-email">
                <?php echo ipSlot('text', array('id' => 'top-email', 'tag' => 'span', 'default' => ('klasera@klasera.lt'), 'class' => 'top-email')); ?>
            </div>
        </div>
        <div class="landing-content">
            <a href="https://www.klasera.lt/lt/bosch/">
                <div class ="landing block1">
                    <?php //echo ipBlock('landing1')->render();?>
                    <img class="landing-image" src="https://www.klasera.lt/file/repository/landing1.png" alt="">

                    <div class="block-text">
                        <img src="<?php echo ipThemeUrl('images/bosh.png'); ?>" alt="bosh" />
                        <h1>servisas</h1>
                    </div>
                </div>
            </a>

            <a href="https://www.klaipedosaudicentras.audi.lt/lt.html?rgp" target="_blank">
                <div class ="landing block2">
                    <?php //echo ipBlock('landing2')->render();?>
                    <img class="landing-image" src="https://www.klasera.lt/file/repository/landing2_1.jpg" alt="">

                    <div class="block-text">
                        <h1>Klaipėdos</h1>
                        <img style="margin-right:20px; margin-bottom:10px;" src="<?php echo ipThemeUrl('images/audi.png'); ?>" alt="audi" />
                        <h1 style="float:left;">centras</h1>
                    </div>
                </div>
            </a>

            <a href="https://www.klasera.lt/lt/klasera/">
                <div class ="landing block3">
                    <?php //echo ipBlock('landing3')->render();?>
                    <img class="landing-image" src="https://www.klasera.lt/file/repository/landing3.png" alt="">

                    <div id="ipBlock-landing3-text" class="block-text">
                        <img src="<?php echo ipThemeUrl('images/klasera-landing.png'); ?>" alt="klasera" />
                        <h1>sprendimai</h1>
                    </div>
                </div>
            </a>
        </div>
    <div class="clear"></div>
<?php echo ipView('_footer.php')->render(); ?>

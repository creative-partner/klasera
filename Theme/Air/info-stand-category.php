<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <link rel="stylesheet" href="<?php echo ipThemeUrl('styles/deals.css') ?>">
    <link rel="shortcut icon" href="https://www.klasera.lt/bosch.ico">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
</head>
<body class="<?php echo ipContent()->getCurrentLanguage()->getTextDirection() ?>">

<div class="container deals">
    <div class="row">
        <?php echo ipBlock('main')->render(); ?>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        mainBack();
    });

    function mainBack() {
        var t;
        window.onload = resetTimer;
        window.onmousemove = resetTimer;
        window.onmousedown = resetTimer;  // catches touchscreen presses as well
        window.ontouchstart = resetTimer; // catches touchscreen swipes as well
        window.onclick = resetTimer;      // catches touchpad clicks as well
        window.onkeypress = resetTimer;
        window.addEventListener('scroll', resetTimer, true); // improved; see comments

        function redirectMain() {
            window.location = '/lt/info-stendas';
        }

        function resetTimer() {
            clearTimeout(t);
            t = setTimeout(redirectMain, 600000);  // time is in milliseconds
        }
    }
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71698335-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>

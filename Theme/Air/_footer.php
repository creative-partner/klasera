<footer class="clearfix klasera-footer">
    <div class="col_12">
        <div class="header-wrapp">
            <div class="left">
                <div class="row-cols">
            <?php //
            //echo ipSlot('text', array('id' => 'themeName', 'tag' => 'div', 'default' => __('Theme "Air"', 'Air', false), 'class' => 'left'));
            ?>
                    <div class="col-address">
                    <?php
                        echo ipBlock('adresas')->asStatic()->render();
                    ?>
                    </div>

                    <div class="col-phone">
                     <?php
                        echo ipBlock('telefonas')->asStatic()->render();
                     ?>
                    </div>

                    <div class="col-email">
                     <?php
                        echo ipBlock('pastas')->asStatic()->render();
                     ?>
                    </div>

                    <div class="col-policy">
                     <?php
                        echo ipBlock('policy')->asStatic()->render();
                     ?>
                    </div>

                <a href="#"><span class="fb-icon"></span></a>
                </div>
            </div>
        </div>
        <div class="right">
            <?php
			echo ipBlock('copyright')->asStatic()->render();?>


			<div class="solution">
                <a href="https://saskaita123.lt/" target="_blank">Sukūrė:</a>
                <a href="https://cpartner.lt" target="_blank"><span class="creative-partner"></span></a>
            </div>

        </div>
    </div>
</footer>
</div>
<?php echo ipView('_footer_cookies.php')->render(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71698335-1', 'auto');
  ga('send', 'pageview');

</script>
<?php echo ipAddJs('assets/site.js'); ?>
<?php echo ipAddJs('assets/scripts.js'); ?>
<?php echo ipJs(); ?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
/*    $(document).ready(function(){
        $(".privacy").colorbox({
            width:"60%",
            className: 'privacy-form',
            html:`<?php echo ipBlock('privacyText')->asStatic()->render(); ?> <a href="#" onclick="privacyAgree()" class="btn btn-default privacy-agree" >Sutinku ir susipažinau</a>`,
        });
    });*/

    function privacyAgree(){
        $(".checkbox input[type='checkbox']").prop('checked', true);
        parent.$.fn.colorbox.close();
        return false;
    }
</script>
<script>
    jQuery( document ).ready( function( $ ) {
/*        if(getCookie('analytic_cookies')){
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-F3YSGMXYPF');
        }*/
        jQuery( "#open-settings" ).click(function(e) {
            jQuery('#cookie-primary').removeClass('cookie-agreement-open');
            jQuery('#cookie-agreement').toggleClass('cookie-agreement-open');
            e.preventDefault();
        });
        jQuery( ".cookie-floater" ).click(function(e) {
            e.preventDefault();
            jQuery('#cookie-agreement').toggleClass('cookie-agreement-open');
            jQuery('#cookie-overlay').toggleClass('cookie-agreement-open');
            e.preventDefault();
        });
    });
</script>

</body>
</html>
